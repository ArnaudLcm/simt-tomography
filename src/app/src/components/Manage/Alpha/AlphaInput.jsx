import { Button } from "../../Utils/Button.styles";
import {useState, useEffect} from "react"
import {MdModeEditOutline} from "react-icons/md"
import {RiSave3Fill} from "react-icons/ri"

export default function AlphaInput({alphas, dispatcher, setErrors}) {

    const [edit, setEdit] = useState(false)

    const [alphasString, setAlphasString] = useState("")

    const updateStateEdit  = (state) => {
        if(state) {
            setEdit(true)
        } else {
            if(alphasString.length === 0) {
                dispatcher([])
                setEdit(false)
                return
            }

            const alphasSplitted = alphasString.split(',')
            const newAlphas = []
            const errs = {}
            for (const splitted of alphasSplitted) {
                if (isNaN(parseFloat(splitted))) {
                   errs['alphas'] = "Alphas are not well formated. Please do follow this example : 100, 20, 40, 15, 60"
                    break;
                } else {
                    newAlphas.push(parseFloat(splitted))
                }
            }

            if(Object.keys(errs).length === 0) {
                dispatcher(newAlphas)
                setEdit(false)
            }
            setErrors(errs)
        }
    }

    useEffect(() => {
        setAlphasString((prev) => alphas?.reduce((acc, alpha, i) => {
            let result = acc;
            if(i !== 0) {
                result +=","
            }
            return result += alpha


        }, "") ?? "")
    }, [alphas])

    return (
        <div style={{display: "flex", gap: "10px", width: "100%"}}>
            <div style={{flexGrow: 1, marginRight: "30px"}}>
                <input role="textbox" placeholder={"Your alphas"} onChange={(e) => setAlphasString(e.target.value)} disabled={!edit} value={alphasString} />
            </div>
            <Button onClick={() => updateStateEdit(!edit)}  style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
                {edit ? (
                    <RiSave3Fill />
                ): (
                    <MdModeEditOutline size={18} />
                )}
            </Button>
        </div>
    )

}