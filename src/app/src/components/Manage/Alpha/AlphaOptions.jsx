import { useState } from "react";
import { useEffect } from "react";
import { useConfigStore } from "../../../store/configStore";
import { OptionsMenu } from "../Manage.styles";
import AlphaInput from "./AlphaInput";
import { Button } from "../../Utils/Button.styles";
import "./AlphaInput.css"
import NotificationManager from "react-notifications/lib/NotificationManager";
import DisplayErrors from "../../Utils/DisplayErrors";
import {validateConfig} from "../../../libs/ConfigValigator";
import Loader from "../../Utils/Loader";

export default function AlphaOptions() {

    const {getConfig, updateConfig} = useConfigStore((state) => state)
    const [processedConfig, setProcessedConfig] = useState();

    const [errs, setErrs] = useState({})

    const [isLoading, setIsLoading] = useState(false)

    const [hasBeenUpdated, setHasBeenUpdated] = useState(false)
    const dispatcher = (newAlphas) => {
        processedConfig.alphas = newAlphas
        setProcessedConfig((prev) => processedConfig)
        setHasBeenUpdated(true)
    }

    const persistModifications = () => {
        setIsLoading(true)
        try {
            const config = JSON.parse(getConfig())
            config.alphas = processedConfig.alphas;
            const err = validateConfig(config)
            setErrs((prev) => err)

            if (Object.keys(err).length === 0) {
                updateConfig(JSON.stringify(config))
                setHasBeenUpdated(false)
                NotificationManager.success("The configuration has been successfully updated.")
            }
            setIsLoading(false)
        } catch (error) {
            console.error("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }
    }

    useEffect(() => {
        const cf = JSON.parse(getConfig())
        setProcessedConfig((prev) => cf)
    }, [getConfig()])

    return (
        <OptionsMenu>
            <div style={{display: "flex", flexDirection: "column", gap: "10px", height: "100%", position: "relative"}}>
                <Loader isLoading={isLoading} />
                <h2>Alphas</h2>
                <DisplayErrors errors={errs} />
                <div style={{marginTop: "20px", flexGrow: 1}}>
                    <AlphaInput dispatcher={dispatcher} alphas={processedConfig?.alphas} setErrors={setErrs} />
                </div>
                <div>
                    {hasBeenUpdated && (
                        <Button disabled={!hasBeenUpdated} onClick={persistModifications} style={{ fontSize: "20px", textAlign: "center" }}>
                            Persist modifications
                        </Button>
                    )}
                </div>
            </div>
        </OptionsMenu>

    )

}