import { OptionsMenu } from "./Manage.styles";
import { InputInline } from "../Utils/Inputs.style";
import Range from "../Utils/Range/Range";
import { useCallback, useEffect, useState } from "react";
import Loader from "../Utils/Loader";
import { useConfigStore } from "../../store/configStore";
import BoolInput from "../Utils/BoolInput/BoolInput";
import { NotificationManager } from "react-notifications"
import { Button } from "../Utils/Button.styles";
import { validateConfig } from "../../libs/ConfigValigator"
import { Alert } from "../Utils/Alerts/alert.styles";

export default function FormOptions() {

    const { updateConfig, getConfig } = useConfigStore()

    const [isLoading, setIsLoading] = useState(true)

    const [showShape, setShowShape] = useState(true)

    const [showElectrodes, setShowElectrodes] = useState(true)

    const [positionX, setPositionX] = useState(0)

    const [positionY, setPositionY] = useState(0)

    const [nbrElectrodes, setNbrElectrodes] = useState(0)

    const [nbrDeformationPoint, setNbrDeformationPoint] = useState(0)

    const [electrodeLength, setElectrodeLength] = useState(0)


    const [hasBeenUpdated, setHasBeenUpdated] = useState(false)

    const [errors, setErrors] = useState([])

    useEffect(() => {
        try {
            const config = JSON.parse(getConfig())
            setShowShape(config.showShape ?? true)
            setShowElectrodes(config.showElectrodes ?? true)
            setPositionX(config.positionX ?? 0)
            setPositionY(config.positionY ?? 0)
            setNbrElectrodes(config.nbrElectrodes ?? 0)
            setElectrodeLength(config.electrodeLength ?? 0)
            setNbrDeformationPoint(config.nbrDeformationPoint ?? 0)
        } catch (error) {
            console.error("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }
        setIsLoading(false)
    }, [])

    const persistModifications = useCallback(() => {
        setIsLoading(true)
        try {
            const config = JSON.parse(getConfig())
            config.showShape = showShape;
            config.showElectrodes = showElectrodes;
            config.positionX = positionX;
            config.positionY = positionY;
            config.nbrElectrodes = nbrElectrodes;
            config.electrodeLength = electrodeLength;
            config.nbrDeformationPoint = nbrDeformationPoint
            const err = validateConfig(config)
            setErrors((prev) => err)

            if (Object.keys(err).length == 0) {
                updateConfig(JSON.stringify(config))
                NotificationManager.success("The configuration has been successfully updated.")
                setHasBeenUpdated(false)
            }
            setIsLoading(false)
        } catch (error) {
            console.error("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }
    })

    const handleUpdate = useCallback((value, fncUpdate) => {
        setHasBeenUpdated(true)

        return fncUpdate(value)

    })

    return (
        <OptionsMenu>
            <Loader isLoading={isLoading} />
            {!isLoading && (
                <>
                    <h2>Forms Settings</h2>
                    {Object.keys(errors).map(key => errors[key]).flat().length > 0 && (
                        <Alert>
                            <div className="alert error-alert">
                                <p>Please fix the following errors :</p>
                                <span>
                                    <ul style={{ listStyle: "inside" }}>
                                        {Object.keys(errors).map(key => errors[key]).flat().map((err, i) => (
                                            <li key={i}>{err}</li>
                                        ))}
                                    </ul>
                                </span>
                            </div>


                        </Alert>
                    )}
                    <div style={{ margin: "25px 0", display: "flex", flexDirection: "column", gap: "10px" }}>
                        <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                            <InputInline error={(errors && errors["showShape"])} style={{ padding: "10px" }}>
                                <p style={{ fontSize: "20px" }}>Show Shape</p>
                            </InputInline>
                            <BoolInput val={showShape} setVal={(v) => handleUpdate(v, setShowShape)} />
                        </div>
                        <InputInline error={(errors && (errors["positionX"] || errors["positionY"]))} style={{ padding: "10px" }}>
                            <p style={{ fontSize: "20px" }}>Position</p>
                            <div style={{ display: "flex", gap: "5px" }}>
                                <Range placeHolder={"X"} val={positionX} setVal={(v) => handleUpdate(v, setPositionX)} />
                                <Range placeHolder={"Y"} val={positionY} setVal={(v) => handleUpdate(v, setPositionY)} />
                            </div>
                        </InputInline>
                        <InputInline error={(errors && errors["nbrDeformationPoint"])} style={{ padding: "10px" }}>
                            <p style={{ fontSize: "20px" }}>Number of deformation point</p>
                            <div style={{ display: "flex", gap: "5px" }}>
                                <Range placeHolder={"N"} val={nbrDeformationPoint} setVal={(v) => handleUpdate(v, setNbrDeformationPoint)} />
                            </div>
                        </InputInline>
                        <InputInline error={(errors && errors["nbrElectrodes"])} style={{ padding: "10px" }}>
                            <p style={{ fontSize: "20px" }}>Electrodes</p>
                            <div style={{ display: "flex", gap: "5px" }}>
                                <Range placeHolder={"N"} val={nbrElectrodes} setVal={(v) => handleUpdate(v, setNbrElectrodes)} />
                            </div>
                        </InputInline>
                        <InputInline error={(errors && errors["electrodeLength"])} style={{ padding: "10px" }}>
                            <p style={{ fontSize: "20px" }}>Electrode Length</p>
                            <div style={{ display: "flex", gap: "5px" }}>
                                <Range placeHolder={"N"} val={electrodeLength} setVal={(v) => handleUpdate(v, setElectrodeLength)} step={0.1} />
                            </div>
                        </InputInline>
                        <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                            <InputInline error={(errors && errors["showElectroes"])} style={{ padding: "10px" }}>
                                <p style={{ fontSize: "20px" }}>Show Electrodes</p>
                            </InputInline>
                            <BoolInput val={showElectrodes} setVal={(v) => handleUpdate(v, setShowElectrodes)} />
                        </div>
                        {hasBeenUpdated && (
                            <Button onClick={persistModifications} style={{ fontSize: "20px", textAlign: "center" }}>
                                Persist modifications
                            </Button>
                        )}
                    </div>
                </>
            )}

        </OptionsMenu>

    )
}