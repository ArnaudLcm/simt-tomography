import { useEffect, useState } from "react";
import { SplineShape, Electrode } from "../../libs/SplineShape";
import Sketch from "react-p5";
import { getShapePos } from "../../libs/Utils";
import {useConfigStore} from "../../store/configStore"
import Loader from "../Utils/Loader";
import _ from  "lodash"
import { ZoomOptions } from "./ZoomOption.styles";
import {TbZoomPan} from "react-icons/tb"
import {GrPowerReset} from "react-icons/gr"
import {AiOutlineDownload} from "react-icons/ai"
import { HelperItem, HelperWrapper } from "./Manage.styles";
import NotificationManager from "react-notifications/lib/NotificationManager";

export default function Graph({hasActiveMenu}) {

    const { updateConfig ,getConfig } = useConfigStore((state) => state)
    const [processedConfig, setProcessedConfig] = useState();
    const [currentControlPoint, setCurrentControlPoint] = useState(null)
    const [P5Instance, setP5Instance] = useState(null)
    const [formInstance, setFormInstance] = useState(null)
    const [hasBeenInit, setHasBeenInit] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    const [scale, setScale] = useState(1.0)


    const handleSketchClicked = (e) => {
        if(formInstance != null) {
            const cp = formInstance.getControlPointAt(e.mouseX, e.mouseY);
            setCurrentControlPoint(cp)        
        }
    }

    const handleSketchDragged = (e) => {
        if(Number.isInteger(currentControlPoint) && formInstance != null) {
            formInstance.moveControlPoint(currentControlPoint, e.mouseX, e.mouseY)
            setHasBeenInit(false)
            P5Instance.redraw()
        }
    }


    const setup = (p5, canvasParentRef) => {
		p5.createCanvas(window.innerWidth, window.innerHeight).parent(canvasParentRef);

	}

    const handleSketchKeyPressed = (e) => {
        if(e.key === "r") {
            if(scale != 1.0) {
                setScale(1.0)
            }
        }

        if(e.key === " ") {
            if(formInstance != null) {
                formInstance.displayControlPoints = !formInstance.displayControlPoints;
            }
            P5Instance.redraw()
        }
    }

    const onMouseWheel = (e) => {
        if(e._mouseWheelDeltaY > 0 && !hasActiveMenu) {
            setScale((prev) => Number((prev + 0.05).toFixed(2)))
        }
    }

    const draw = (p5) => {
        p5.background(255);
        if(processedConfig && processedConfig.gridSize > 0) { // Prevent the division by 0
            for (var x = 0; x < p5.width; x += p5.width / (processedConfig.gridSize*2)) {
                for (var y = 0; y < p5.height; y += p5.height / (processedConfig.gridSize*2)) {
                    p5.stroke("#838383");
                    p5.strokeWeight(1);
                    p5.line(x, 0, x, p5.height);
                    p5.line(0, y, p5.width, y);
                }
            }
            if(processedConfig.showAxis) {
                p5.stroke(0)
                p5.strokeWeight(4)
                p5.line(0, p5.height/2, p5.width, p5.height/2)
                p5.line(p5.width/2, 0, p5.width/2, p5.height)
                p5.fill(0)
                p5.triangle(p5.width-30, p5.height/2+10, p5.width-30, p5.height/2-10, p5.width, p5.height/2)
                p5.triangle(p5.width/2-10, 30, p5.width/2+10, 30, p5.width/2, 0)
            }

            if(processedConfig && processedConfig.showShape) {
                p5.noFill()
                if(!hasBeenInit) {
                    const circle = new SplineShape(100, getShapePos(p5.width, processedConfig.gridSize, processedConfig.positionX), getShapePos(p5.height, processedConfig.gridSize, -processedConfig.positionY), parseInt(processedConfig.nbrDeformationPoint), p5);
                    
                    p5.stroke(0,0,255);
                    p5.strokeWeight(7);

                    if(processedConfig.alphas && processedConfig.alphas.length > 0) {
                        circle.makeFromAlphas(processedConfig.alphas)
                    }

                    if(processedConfig.showElectrodes) {
                        for (let index = 0; index < processedConfig.nbrElectrodes; index++) {
                            const rightTheta = circle.getElectrodeRightAngle((index*2*Math.PI)/processedConfig.nbrElectrodes, processedConfig.electrodeLength)
                            if(isNaN(rightTheta)) {
                                NotificationManager.error("Your configuration is not correct: Alphas or electrodes are misconfigured.")
                                break;
                            }
                            circle.electrodes.push(new Electrode(  (index*2*Math.PI)/processedConfig.nbrElectrodes,  rightTheta))
                        }
                    }

                    setFormInstance(circle)  
                    circle.display()     
                }
                if(formInstance != null) {
                    formInstance.display()  

                }
                setP5Instance(p5)
            }

            setHasBeenInit(true)
        }
            p5.noLoop()
	}

    const persistAlphas = () => {
        setIsLoading(true)
        if(P5Instance != null && formInstance != null) {
            const alphas = formInstance.resolveAlphas(processedConfig.nbrDeformationPoint)

            const config = JSON.parse(getConfig())
            config.alphas = alphas
            updateConfig(JSON.stringify(config))

            NotificationManager.success("Alphas has been succesfully calculated.")
        }
        setIsLoading(false)
    }

    useEffect(() => {
        const cf = JSON.parse(getConfig())
        setProcessedConfig((prev) => cf)
        setHasBeenInit(false)
    }, [getConfig()])

    useEffect(() => {
        if(P5Instance !== null) {
            setIsLoading(true)
            P5Instance.draw()
            setIsLoading(false)
        }
    }, [processedConfig, hasBeenInit])


    useEffect(() => {
        if(P5Instance !== null) {
            if(scale == 1.0) {
                P5Instance.redraw()

            } else {
                P5Instance.translate(P5Instance.mouseX, P5Instance.mouseY)
                P5Instance.scale(scale)
                P5Instance.translate(-P5Instance.mouseX, -P5Instance.mouseY)
                P5Instance.draw()
            }
        }
    }, [scale])

    return (
        <>
            <Sketch 
                keyPressed={handleSketchKeyPressed}
                mouseDragged={handleSketchDragged}
                mousePressed={handleSketchClicked}
                mouseWheel={_.debounce(onMouseWheel, 25)}
                setup={setup}
                draw={draw}
            />
            {isLoading && (
            <div style={{position: "absolute", width: "100%", height: "100%", zIndex: 100, backdropFilter: "blur(5px)", display: "flex", justifyContent: "center", alignItems: "center"}}>
                <Loader isLoading={isLoading} />
            </div> 
            )}
                <HelperWrapper style={{right: "120px"}}>
                    <HelperItem onClick={persistAlphas}>
                        <AiOutlineDownload />
                    </HelperItem>
                </HelperWrapper>
            {scale !== 1.0 && (
                <ZoomOptions>
                    <div className="content">
                        <TbZoomPan size={28} />
                        <p>x{scale}</p>
                    </div>
                    <HelperItem onClick={() => setScale(1.0)}>
                        <GrPowerReset size={28} />
                    </HelperItem>
                </ZoomOptions>

            )}
   
        </>
    )

}