import { OptionsMenu } from "./Manage.styles";
import { InputInline } from "../Utils/Inputs.style";
import Range from "../Utils/Range/Range";
import { useCallback, useEffect, useState } from "react";
import Loader from "../Utils/Loader";
import { useConfigStore } from "../../store/configStore";
import BoolInput from "../Utils/BoolInput/BoolInput";
import { NotificationManager } from "react-notifications"
import { Button } from "../Utils/Button.styles";
import { validateConfig } from "../../libs/ConfigValigator";
import { Alert } from "../Utils/Alerts/alert.styles";
import DisplayErrors from "../Utils/DisplayErrors";

export default function GridOptions() {

    const { updateConfig, getConfig } = useConfigStore()

    const [isLoading, setIsLoading] = useState(true)

    const [gridSize, setGridSize] = useState(0)
    const [showAxis, setShowAxis] = useState(true)

    const [hasBeenUpdated, setHasBeenUpdated] = useState(false)

    const [errors, setErrors] = useState([])

    useEffect(() => {
        try {
            const config = JSON.parse(getConfig())
            setGridSize(config.gridSize ?? 0)
            setShowAxis(config.showAxis ?? true)
        } catch (error) {
            console.error("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }
        setIsLoading(false)
    }, [])

    const persistModifications = useCallback(() => {
        setIsLoading(true)
        try {
            const config = JSON.parse(getConfig())
            config.gridSize = gridSize;
            config.showAxis = showAxis;
            
            const err = validateConfig(config)
            setErrors((prev) => err)
            if(Object.keys(err).length == 0) {
                updateConfig(JSON.stringify(config))
                NotificationManager.success("The configuration has been successfully updated.")
                setHasBeenUpdated(false)
            }
            setIsLoading(false)
        } catch (error) {
            console.error("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }
    })

    const handleUpdate = useCallback((value, fncUpdate) => {
        setHasBeenUpdated(true)

        return fncUpdate(value)

    })

    return (
        <OptionsMenu>
            <Loader isLoading={isLoading} />
            {!isLoading && (
                <>
                    <h2>Grid Settings</h2>
                    <DisplayErrors errors={errors} />
                    <div style={{ margin: "25px 0", display: "flex", flexDirection: "column", gap: "10px" }}>
                        <InputInline error={(errors && errors["gridSize"])} style={{ padding: "10px" }}>
                            <p style={{ fontSize: "20px" }}>Grid Size</p>
                            <div style={{ display: "flex", gap: "5px" }}>
                                <Range placeHolder={"Y"} val={gridSize} setVal={(v) => handleUpdate(v, setGridSize)}  />
                            </div>
                        </InputInline>
                        <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                            <InputInline onError={(errors && errors["showAxis"])} style={{ padding: "10px" }}>
                                <p style={{ fontSize: "20px" }}>Show Axis</p>
                            </InputInline>
                            <BoolInput val={showAxis}  setVal={(v) => handleUpdate(v, setShowAxis)}  />
                        </div>
                        {hasBeenUpdated && (
                            <Button onClick={persistModifications} style={{ fontSize: "20px", textAlign: "center" }}>
                                Persist modifications
                            </Button>
                        )}
                    </div>
                </>
            )}

        </OptionsMenu>

    )
}