import { AlphaIcon, ShapeIcon } from "../Utils/Icons";
import { ManageWrapper, SectionMenu, SectionMenuItem } from "./Manage.styles";
import {MdGrid4X4} from "react-icons/md"
import { useState } from "react";
import GridOptions from "./GridOptions";
import FormOptions from "./FormOptions";
import Solver from "./Solver";
import ResetMenu from "./ResetMenu";
import Graph from "./Graph";
import AlphaOptions from "./Alpha/AlphaOptions";

export default function Manage() {


    const [activeMenu, setActiveMenu] = useState(null)

    const updateActiveMenu = (id) => {
        if(activeMenu === id) {
            setActiveMenu(null)
        } else {
            setActiveMenu(id)
        }
    }

    return (
        <ManageWrapper>
            <SectionMenu>
                <SectionMenuItem isActive={activeMenu === "grid"} onClick={() => updateActiveMenu("grid")}>
                    <MdGrid4X4 size={36} />
                </SectionMenuItem>
                <SectionMenuItem isActive={activeMenu === "shape"} onClick={() => updateActiveMenu("shape")}>
                    <ShapeIcon />
                </SectionMenuItem>
                <SectionMenuItem isActive={activeMenu === "alpha"} onClick={() => updateActiveMenu("alpha")}>
                    <AlphaIcon />
                </SectionMenuItem>
                <SectionMenuItem onClick={() => updateActiveMenu("solver")} isActive={activeMenu === "solver"}>
                    Solve
                </SectionMenuItem>
            </SectionMenu>
            <Graph hasActiveMenu={activeMenu != null} />
            {activeMenu === "grid" && (
                <GridOptions />
            )}
            {activeMenu === "shape" && (
                <FormOptions />
            )}
            {activeMenu === "alpha" && (
                <AlphaOptions />
            )}
            {activeMenu === "solver" && (
                <Solver />
            )}
            <ResetMenu />
        </ManageWrapper>
    )
}