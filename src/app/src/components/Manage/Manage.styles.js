import styled from "styled-components"


export const ManageWrapper = styled.div`
    display: flex;
    height: 100vh;
    width: 100%;
    position: relative;
`

export const SectionMenu = styled.div`
    background: rgba(117, 202, 250, 0.83);
    border: 1px solid #000000;
    border-radius: 20px;
    position: absolute;
    top: 25px;
    left: 41px;
    width: 107px;
    display: flex;
    align-items: center;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    flex-direction: column;
    gap: 10px;
    padding: 10px 0;
    @media only screen and (max-width: 768px) {
        width: 70px;
    }

`

export const SectionMenuItem = styled.div`
    width: 83px;
    height: 82px;
    background: ${props => props.isActive ? "#cfd7df" : "#F5FAFF"};
    border: 2px solid rgba(0, 0, 0, 0.75);
    border-radius: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: "Inconsolata-Bold";
    font-size: 24px;
    cursor: pointer;

    & svg {
        height: 48px;
        width: 48px;
    }
    @media only screen and (max-width: 768px) {
        & svg {
            width: 24px;
            height: 24px;
        }
        width: 43px;
        height: 42px;
        font-size: 12px;


    }


    &:hover {
        background: #cfd7df;
    }
`

export const OptionsMenu = styled.div`
    background: rgba(117, 202, 250, 0.83);
    border: 1px solid #000000;
    border-radius: 20px;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    position: absolute;
    bottom: 25px;
    left: 41px;
    overflow-y: auto;
    width: 500px;
    max-height: 400px;
    min-height: 400px;
    align-items: center;
    flex-direction: column;
    z-index: 50;
    gap: 10px;
    padding: 15px;
    font-family: "Inconsolata-Bold";

    h2 {
        font-size: 26px;
    }
`

export const HelperWrapper = styled.div`
    background: rgba(117, 202, 250, 0.83);
    border: 1px solid #000000;
    border-radius: 20px;
    position: absolute;
    bottom: 25px;
    right: 41px;
    align-items: center;
    flex-direction: column;
    padding: 5px;
    font-family: "Inconsolata-Bold";
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

`

export const HelperItem = styled.div`
    width: 43px;
    height: 42px;
    background: #F5FAFF;
    border: 2px solid rgba(0, 0, 0, 0.75);
    border-radius: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: "Inconsolata-Bold";
    font-size: 24px;
    cursor: pointer;


    &:hover {
        background: #cfd7df;
    }
`