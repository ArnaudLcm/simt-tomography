import { HelperItem, HelperWrapper } from "./Manage.styles";
import {MdResetTv} from "react-icons/md"
import { useCallback } from "react";
import { useConfigStore } from "../../store/configStore";

export default function ResetMenu() {

    const {updateConfig, getConfig} = useConfigStore()

    const handleReset = useCallback(() => {
        localStorage.removeItem("simt")
        updateConfig(null)
    })

    return (
        <HelperWrapper>
            <HelperItem onClick={handleReset}>
                <MdResetTv />
            </HelperItem>
        </HelperWrapper>
    )
}