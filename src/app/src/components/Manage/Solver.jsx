import { useCallback, useEffect, useState } from "react";
import { OptionsMenu } from "./Manage.styles";
import Loader from "../Utils/Loader";
import { InputInline } from "../Utils/Inputs.style";
import { useConfigStore } from "../../store/configStore";
import Range from "../Utils/Range/Range";
import { Button } from "../Utils/Button.styles";
import { NotificationManager } from 'react-notifications';
import Axios from "axios"
import { validateConfig } from "../../libs/ConfigValigator";
import { Alert } from "../Utils/Alerts/alert.styles";
import DisplayErrors from "../Utils/DisplayErrors";


export default function Solver() {

    const [isLoading, setIsLoading] = useState(true)

    const { updateConfig, getConfig } = useConfigStore()

    const [gridSize, setGridSize] = useState(0)

    const [nbrElectrodes, setNbrElectrodes] = useState(0)

    const [electrodeLength, setElectrodeLength] = useState(0)

    const [hasBeenUpdated, setHasBeenUpdated] = useState(false)

    const [errors, setErrors] = useState([])

    useEffect(() => {
        try {
            const config = JSON.parse(getConfig())
            setGridSize(config.gridSize ?? 0)
            setNbrElectrodes(config.nbrElectrodes ?? 0)
            setElectrodeLength(config.electrodeLength ?? 0)
        } catch (error) {
            console.error("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }

        setIsLoading(false)

    }, [getConfig()])

    const persistModifications = useCallback(() => {
        setIsLoading(true)
        try {
            const config = JSON.parse(getConfig())
            config.gridSize = gridSize;
            config.nbrElectrodes = nbrElectrodes;
            config.electrodeLength = electrodeLength;

            const err = validateConfig(config)

            setErrors((prev) => err)
            if (Object.keys(err).length === 0) {
                updateConfig(JSON.stringify(config))
                NotificationManager.success("The configuration has been successfully updated.")
                setHasBeenUpdated(false)
            }
            setIsLoading(false)
        } catch (error) {
            console.log("Can't parse the config from local storages.")
            NotificationManager.error("An error occured while trying to retrieve the config.")
        }
    })

    const handleUpdate = useCallback((value, fncUpdate) => {
        setHasBeenUpdated(true)

        return fncUpdate(value)

    })

    const processSolving = useCallback(() => {
        setIsLoading(true)
        Axios.post(process.env.REACT_APP_ENDPOINT_API_RESOLVER).catch(e => {
            NotificationManager.error("An error occured while trying to call the resolving endpoint.")
        }).finally(() => setIsLoading(false))
    })

    const exportConfig = useCallback(() => {
        const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
            getConfig()
        )}`;
        const link = document.createElement("a");
        link.href = jsonString;
        link.download = "simtConfig.json";

        link.click();
    })



    return (
        <OptionsMenu>
            <Loader isLoading={isLoading} />
            <h2>Solver</h2>
            <DisplayErrors errors={errors} />
            <div style={{ margin: "25px 0", display: "flex", flexDirection: "column", gap: "10px" }}>
                <InputInline error={(errors && errors["gridSize"])}>
                    <p style={{ fontSize: "20px" }}>Grid Size</p>
                    <div style={{ display: "flex", gap: "5px" }}>
                        <Range placeHolder={"Y"} val={gridSize} setVal={(v) => handleUpdate(v, setGridSize)} />
                    </div>
                </InputInline>
                <InputInline error={(errors && errors["nbrElectrodes"])}>
                    <p style={{ fontSize: "20px" }}>Electrodes</p>
                    <div style={{ display: "flex", gap: "5px" }}>
                        <Range placeHolder={"N"} val={nbrElectrodes} setVal={(v) => handleUpdate(v, setNbrElectrodes)} />
                    </div>
                </InputInline>
                <InputInline error={(errors && errors["electrodeLength"])}>
                    <p style={{ fontSize: "20px" }}>Electrodes length</p>
                    <div style={{ display: "flex", gap: "5px" }}>
                        <Range placeHolder={"N"} val={electrodeLength} setVal={(v) => handleUpdate(v, setElectrodeLength)} step={0.1} />
                    </div>
                </InputInline>
                {hasBeenUpdated && (
                    <Button onClick={persistModifications} style={{ fontSize: "20px", textAlign: "center" }}>
                        Persist modifications
                    </Button>
                )}

                <div style={{ display: "grid", gridTemplateColumns: "70% 25%", gap: "20px", marginTop: "20px" }}>
                    <Button onClick={processSolving} style={{ fontSize: "20px" }}>
                        Compute Solution
                    </Button>

                    <Button style={{ fontSize: "20px" }} onClick={exportConfig}>
                        Export...
                    </Button>
                </div>
            </div>
        </OptionsMenu>
    )
}