import styled from "styled-components";


export const ZoomOptions = styled.div`
    background: rgba(117, 202, 250, 0.83);
    border: 1px solid #000000;
    border-radius: 20px;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    position: absolute;
    bottom: 25px;
    left: 41px;
    align-items: center;
    display: flex;
    gap: 10px;
    padding: 10px;
    font-family: "Inconsolata-Bold";

    h2 {
        font-size: 26px;
    }

    & .content {
        background: white;
        padding: 10px;
        border-radius: 15px;
        display: flex;
        flex-direction: "row";
        align-items: center;
        gap: 5px;
    }

`