import { useState } from "react";
import { useEffect } from "react";
import Start from "./Start/Start";
import Manage from "./Manage/Manage"
import Loading from "./Loading";
import { useConfigStore } from "../store/configStore";
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

export default function Root() {

    const [isLoading, setIsLoading] = useState(true)
    const { updateConfig, getConfig } = useConfigStore((state) => state)

    useEffect(() => {
        setIsLoading(true)
        const config = localStorage.getItem("simt")
        updateConfig(config === "null" ? null : config)
        setIsLoading(false)
    }, [getConfig])

    return (
        <div style={{overflow: "hidden"}}>
            {isLoading ? (
                <Loading />
            ) : (
                <>
                    <NotificationContainer />
                    {getConfig() ? (
                        <Manage />
                    ) : (
                        <Start />

                    )}
                </>
            )}
        </div>
    )

}