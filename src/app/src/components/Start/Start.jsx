import React, { useReducer, useState } from "react";
import { Button } from "../Utils/Button.styles";
import { InputInline } from "../Utils/Inputs.style";
import Range from "../Utils/Range/Range";
import { StartWrapper } from "./Start.styles";
import { FaMinus, FaPlus } from "react-icons/fa"
import { useConfigStore } from "../../store/configStore";
import Sketch from "react-p5"
import {NotificationManager} from "react-notifications"
import { validateConfig } from "../../libs/ConfigValigator";
import { Alert } from "../Utils/Alerts/alert.styles";
import { HelperItem, HelperWrapper } from "../Manage/Manage.styles";
import {BiHelpCircle} from "react-icons/bi"
import Tooltip from "./Tooltip";
import AlphaInput from "../Manage/Alpha/AlphaInput";
import DisplayErrors from "../Utils/DisplayErrors";

function Start() {

  const draw = (p5) => {
    p5.background("#D9D9D9");

    p5.stroke(0)
    p5.strokeWeight(1)
    p5.line(0, p5.height / 2, p5.width, p5.height / 2)
    p5.line(p5.width / 2, 0, p5.width / 2, p5.height)
    p5.fill(0)
    p5.triangle(p5.width - 30, p5.height / 2 + 10, p5.width - 30, p5.height / 2 - 10, p5.width, p5.height / 2)
    p5.triangle(p5.width / 2 - 10, 30, p5.width / 2 + 10, 30, p5.width / 2, 0)


  };
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(window.innerWidth, window.innerHeight).parent(canvasParentRef);
  };

  const { updateConfig } = useConfigStore((state) => state)
  const [gridSize, setGridSize] = useState(0);
  const [positionX, setPositionX] = useState(0);
  const [positionY, setPositionY] = useState(0);
  const [nbrElectrodes, setNbrElectrodes] = useState(0);
  const [electrodeLength, setElectrodeLength] = useState(0);
  const [nbrDeformationPoint, setNbrDeformationPoint] = useState(8)
  const [errors, setErrors] = useState([]);

  const [displayTooltip, setDisplayTooltip] = useState(false)

  const [alphas, setAlphas] = useState([]);


  const storeParameters = () => {
    const parameters = {
      gridSize: gridSize,
      positionX: positionX,
      positionY: positionY,
      nbrElectrodes: nbrElectrodes,
      electrodeLength: electrodeLength,
      nbrDeformationPoint: nbrDeformationPoint,
      alphas: alphas,
      showAxis: true,
      showShape: true,
      showElectrodes: true,
    }
    const err = validateConfig(parameters)
    setErrors((prev) => err)

    if(Object.keys(err).length === 0) {
      localStorage.setItem("simt", JSON.stringify(parameters))
      updateConfig(localStorage.getItem("simt"))
    }


  }

  const loadConfig = (e) => {
    const fileReader = new FileReader();
    try {
      fileReader.readAsText(e.target.files[0], "UTF-8");
      fileReader.onload = e => {
        const result = JSON.parse(e.target.result);

        const err = validateConfig(result)

        if(result && Object.keys(err).length === 0) {
          localStorage.setItem("simt", JSON.stringify(result))
          updateConfig(localStorage.getItem("simt"))
        } else {
          NotificationManager.error("Your configuration file is not valid.")
        }
      }; 
    } catch (error) {
      console.error("An error occured while parsing the uploaded json file.")

    }
  }

  return (
    <div style={{ background: "white", display: "flex", alignItems: "center", justifyContent: "center", width: "100%", height: "100vh", position: "relative" }} >
      <Sketch draw={draw} setup={setup} style={{position: "absolute", bottom: "0", left: "0"}} />
      <StartWrapper style={{zIndex: 4}}>
        <h1>Start  Project </h1>
        <DisplayErrors errors={errors} />
        <InputInline error={(errors && errors["gridSize"])}>
          <p>Grid Size</p>
          <Range val={gridSize} setVal={setGridSize} placeHolder={"N"} />
        </InputInline>

        <div style={{ display: "flex", flexDirection: "column", gap: "5px"}}>
          <InputInline error={(errors && (errors["positionX"] || errors["positionY"]))}>
            <p>Forms positions</p>
            <div style={{ display: "flex", gap: "5px" }}>
              <Range val={positionX} setVal={setPositionX} placeHolder={"X"} />
              <Range val={positionY} setVal={setPositionY} placeHolder={"Y"} />
            </div>
          </InputInline>
          <div style={{ display: "flex", flexDirection: "column", gap: "10px" }}>
            <InputInline error={(errors && errors["alphas"])} style={{ padding: "10px" }}>
              <p>Forms alphas</p>
            </InputInline>
            <AlphaInput alphas={alphas} dispatcher={setAlphas} setErrors={setErrors} />


          </div>
        </div>
        <InputInline error={(errors && errors["nbrDeformationPoint"])}>
          <p>Number of deformation point</p>
          <Range val={nbrDeformationPoint} setVal={setNbrDeformationPoint} placeHolder={"N"} />
        </InputInline>
        <div style={{ display: "flex", flexDirection: "column", gap: "5px" }}>
          <InputInline error={(errors && errors["nbrElectrodes"])}>
            <p>Electrodes</p>
            <Range val={nbrElectrodes} setVal={setNbrElectrodes} placeHolder={"N"} />
          </InputInline>
          <InputInline error={(errors && errors["electrodeLength"])}>
            <p>Electrode Length</p>
            <Range val={electrodeLength} setVal={setElectrodeLength} placeHolder={"N"} step={0.1} />
          </InputInline>
        </div>


        <Button style={{fontSize: "18px"}} onClick={storeParameters}>Launch With These Settings</Button>
        <Button style={{fontSize: "18px", display: "flex", alignItems: "center"}}>
          <label htmlFor="file">Load Existing Project</label>
          <input id="file" style={{visibility: "hidden", width: "100%"}} accept={"json"} onChange={loadConfig} type="file" />
        </Button>

      </StartWrapper>
      {displayTooltip && (
        <Tooltip />
      )}
      <HelperWrapper>
        <HelperItem onClick={() => setDisplayTooltip((prev) => !prev)}>
          <BiHelpCircle />
        </HelperItem>
      </HelperWrapper>



    </div>

  );
}

export default Start;