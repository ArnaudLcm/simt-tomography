import styled from "styled-components"


export const StartWrapper = styled.div`
    border: 1px solid black; 
    border-radius: 10px;
    background-color: #5CBFF7 ;
    padding: 10px 20px;
    font-family: "Inconsolata-Bold";
    max-height: 70%;
    overflow: auto;
    display: flex;
    width: 25vw;
    flex-direction: column;
    justify-content: center;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    gap: 20px;
    @media only screen and (max-width: 768px) {
        width: 80%;
    }


    h1 {
        font-size: 18px;
        font-family: "Inconsolata-Bold";
    }
`

export const ToolTipWrapper = styled.div`
    background: rgba(117, 202, 250, 0.83);
    border: 1px solid #000000;
    border-radius: 20px;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
    position: absolute;
    bottom: 100px;
    right: 41px;
    width: 500px;
    overflow-y: auto;
    align-items: center;
    flex-direction: column;
    max-height: 35%;
    gap: 10px;
    padding: 15px;
    font-family: "Inconsolata-Bold";

    h2 {
        font-size: 26px;
    }


    & .content {
        background: white;
        padding: 10px;
        border-radius: 15px;
        margin-top: 10px;
    }
`