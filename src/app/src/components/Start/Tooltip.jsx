import { ToolTipWrapper } from "./Start.styles";

export default function Tooltip() {

    return (
        <ToolTipWrapper>
            <h1>Tool tips</h1>
            <div className="content">
                <p>Electrode length : <span style={{color: "#3d3d3d"}}>Length of an electrode in grid's unit.</span></p>
            </div>
            <div className="content">
                <p>Number of deformation point : <span style={{color: "#3d3d3d"}}>Number of point to deform the shape</span></p>
            </div>
            <div className="content">
                <p>Reset Zoom : <span style={{color: "#3d3d3d"}}>You can use key 'R' to reset the zoom.</span></p>
            </div>
            <div className="content">
                <p>Hide deformation points : <span style={{color: "#3d3d3d"}}>You can hide deformation points by pressing 'Space' key.</span></p>
            </div>
            <div className="content">
                <p>Alphas : <span style={{color: "#3d3d3d"}}>Without alphas, the form will be rendered as a perfect circle.</span></p>
            </div>
        </ToolTipWrapper>

    )

}