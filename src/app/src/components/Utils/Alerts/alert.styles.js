import styled from "styled-components";

export const Alert = styled.div`
  .alert {
    color: white;
    border-top-right-radius: 0.375rem;
    border-bottom-right-radius: 0.375rem;
    padding: 1rem;
    margin: 1rem 0;

    span {
      display: flex;
      align-items: center;
      font-size: 14px;
      color: #eaeded;
    }

    svg {
      margin-right: 5px;
      font-size: 17px;
    }
  }


  .error-alert {
    background: rgba(135,0,0);
    border-left: #cb4335 4px solid;
  }

  .error-alert svg {
    color: #dc0000;
  }

  .info-alert {
    background: rgba(40,116,166,0.5);
    border-left: #2874a6 4px solid;
  }

  .info-alert svg {
    color: #2874a6;
  }

  .success-alert {
    background: rgba(146,189,142,0.5);
    border-left: #92bd8e 4px solid;
  }

  .success-alert i {
    color: #92bd8e;
  }

  .warning-alert {
    background: rgba(241,196,15,0.5);
    border-left: #f1c40f 4px solid;
  }

  .warning-alert i {
    color: #f1c40f;
  }

`
