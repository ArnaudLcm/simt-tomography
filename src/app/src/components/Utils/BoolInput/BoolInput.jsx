import { BoolInputWrapper } from "./BoolInput.styles";
import {RxCross1} from "react-icons/rx"

export default function BoolInput({val, setVal}) {
    return(
        <BoolInputWrapper onClick={() => setVal(prev => !prev)}>
            {val && (
                <RxCross1 size={42} />
            )}
        </BoolInputWrapper>
    )
}