import styled from "styled-components"

export const BoolInputWrapper = styled.div`
    width: 61px;
    height: 48px;
    cursor: pointer;
    background: #F1F8FF;
    border: 2.65217px solid rgba(0, 0, 0, 0.75);
    border-radius: 26.5217px;
    display: flex;
    align-items: center;
    justify-content: center;
`