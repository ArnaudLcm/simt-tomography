import styled from "styled-components"

export const NudeButton = styled.div` 
    border: none;
    outline: none;
    background-color: transparent;
    font-size: 17px;
    line-height: 1;
`

export const Button = styled.div` 
    background: white;
    border: 1px solid black;
    border-radius: 20px;
    padding: 5px 10px;
    font-size: 12px;
    cursor: pointer;
    &:hover {
        color: white;
        font-weight: bold;
        background-color: grey;
    }
`