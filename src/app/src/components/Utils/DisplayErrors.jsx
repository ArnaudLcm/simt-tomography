import {Alert} from "./Alerts/alert.styles";

export default function DisplayErrors({errors}) {

    return (
        <>
            {Object.keys(errors).map(key => errors[key]).flat().length > 0 && (
                <Alert>
                    <div className="alert error-alert">
                        <p>Please fix the following errors :</p>
                        <span>
                <ul style={{listStyle: "inside"}}>
                  {Object.keys(errors).map(key => errors[key]).flat().map((err,i) => (
                      <li key={i}>{err}</li>
                  ))}
                </ul>
              </span>
                    </div>


                </Alert>
            )}
        </>
    )


}