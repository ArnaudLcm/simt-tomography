import styled from "styled-components"

export const InputInline = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: white;
    border: ${props => props.error ? "2px solid red" : "1px solid black"};
    border-radius: 20px;
    padding: 0px 10px;
    font-size: 16px;
`