import { RangeWrapper } from "./Range.styles";
import { ChevronDown, ChevronUp } from "../Icons";

export default function Range({val, setVal, placeHolder, step=1}) {


    return (
        <RangeWrapper>
            <input role="textbox" value={val} onChange={(e) => setVal(e.target.value)} />
            <div className="controllers">
                <div onClick={() => setVal((prev) => Math.round((prev+step)*10)/10)}>
                    <ChevronUp />
                </div>
                <div onClick={() => setVal((prev) => Math.round((prev-step)*10)/10)}>
                    <ChevronDown />
                </div>
            </div>
        </RangeWrapper>

    )

}