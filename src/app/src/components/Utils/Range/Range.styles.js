import styled from "styled-components";


export const RangeWrapper = styled.div`
    display: flex;
    align-items: center;
    row-gap: 10px;

    input {
        font-size: 16px;
        border: 1px solid black;
        padding: 10px 15px;
        border-radius: 50%;
        width:18px;
        text-align: center;
    }


    .controllers {
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin-left: 5px;


        svg {
            cursor: pointer;
            color: #BCC4DA;

            &:hover {
                color: #a4acc1;
            }
        }
    }
`