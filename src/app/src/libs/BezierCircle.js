
// approximation of Circle with Bezier Curves
// n is the amount of circular arcs that form the circle
// the minimum has to be 4

import Point from "./Point"
import Curve from "./Curve"
import { distance } from "./Utils";

export default function BezierCircle(radius, x, y, p5, n=4) {
	this.radius = radius;
	this.x = x;
	this.y = y;
	this.d = radius* (4/3) * p5.tan(p5.PI/(2*n));
	this.segments = n;
	this.curves = [];

	this.controlPoints = []
	this.displayControlPoints = true;

	let t = this.d;
	let radius2 = p5.sqrt(t*t + radius*radius);
	let theta = p5.atan(t/radius);
	for(let i = 0; i < this.segments; ++i)
	{
		let anchor = new Point(x+p5.radius*p5.cos(2*p5.PI*i/n), y + p5.radius*p5.sin(2*p5.PI*i/n));
		let endpoint = new Point(x+p5.radius*p5.cos(2*p5.PI*(i+1)/n), y + radius*p5.sin(2*p5.PI*(i+1)/n));
		let control1 = new Point(x+radius2*p5.cos(theta + 2* i * p5.PI/n), y + radius2*p5.sin(theta + 2 * i * p5.PI/n));
		let control2 = new Point(x+radius2*p5.cos(2* (i+1)*p5.PI/n-theta), y + radius2*p5.sin(2*(i+1) *p5.PI/n-theta));

		this.controlPoints.push(anchor);
		this.controlPoints.push(control1);
		this.controlPoints.push(control2);

		this.curves.push(new Curve( anchor,
						 			control1,
						 			control2,
									endpoint,
                                    p5
						 		)
						);
	}

	this.display = function()
	{
		
		p5.stroke(145,10, 200);
		p5.strokeWeight(7);

		if(this.displayControlPoints)
		{
			for(let p = 0; p < this.controlPoints.length; ++p)
			{
				p5.point(this.controlPoints[p].x, this.controlPoints[p].y);
			}
		}

		for(let c = 0; c < this.segments; ++c )
		{
			//anchors
			//stroke(255,0,0);
			//this.curves[c].displayPoints();
			
			// circle with bézier curves
			p5.stroke(42, 157, 143);
			p5.strokeWeight(2);
			//fill(165,105,50);
			p5.noFill();
			this.curves[c].bezierSelf();
		}

	}

	this.getControlPointAt = function(x,y)
	{
		let mindist = this.radius;
		let cp = 0;
		for(let p = 0; p < this.controlPoints.length; ++p)
		{
			let newd = distance(x,y, this.controlPoints[p].x, this.controlPoints[p].y);
			if(newd < mindist)
			{
				mindist = newd;
				cp = p;
			}
		}
		let newd = distance(x,y, this.x, this.y);
		if(mindist < 20)
			return cp;
		if(newd < mindist)
		{
			return new Point(this.x, this.y);
		}
		return null;
	}

	this.moveControlPoint = function(index, x, y)
	{
		const realIndex = Math.floor(index/3);
		if(index%3 === 0)
		{
			this.curves[realIndex].anchor.x = x;
			this.curves[realIndex].anchor.y = y;

			if(index === 0)
			{
				this.curves[this.segments-1].endpoint.x = x;
				this.curves[this.segments-1].endpoint.y = y;
			} else 
			{
				this.curves[realIndex-1].endpoint.x = x;
				this.curves[realIndex-1].endpoint.y = y;
			}
		}
		else if(index%3 === 1)
		{
			this.curves[realIndex].control1.x = x;
			this.curves[realIndex].control1.y = y;
		}	
		else
		{
			this.curves[realIndex].control2.x = x;
			this.curves[realIndex].control2.y = y;
		}		
	}

}