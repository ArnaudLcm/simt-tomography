
/**
 * @brief The purpose of this class is to validate a config file by returning a list of errors
 */
export class ConfigValidator {

    constructor(config) {
        this.errors = {}
        this.config = config
    }    
    isANumber(field) {
        if (isNaN(parseInt(this.config[field])) || this.config[field] % 1 !== 0) {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} is not a number.`]
        }

        return this
    }

    isProvided(field) {
        if (this.config[field] === undefined) {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} is required.`]
        }

        return this
    }

    isAFloat(field) {
        if (isNaN(parseFloat(this.config[field]))) {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} is not a float.`]
        }

        return this
    }

    isABool(field) {
        if (typeof this.config[field] !== "boolean") {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} is not a boolean.`]
        }
        return this
    }

    isPositiveNumber(field) {
        if (this.config[field] < 0) {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} has to be a positive number.`]
        }
        return this
    }

    isStriclySuperior(field, value) {
        if(parseInt(this.config[field]) < value) {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} has to be superior to ${value}.`]
        }

        return this
    }

    isStriclyInferior(field, value) {
        if(parseInt(this.config[field]) > value) {
            this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `The field ${field} has to be inferior to ${value}.`]
        }

        return this
    }

    getErrors() {
        return this.errors
    }




}

export const validateConfig = (config) => {
    return new ConfigValidator(config)
        .isProvided("gridSize")
        .isProvided("positionX")
        .isProvided("positionY")
        .isProvided("nbrElectrodes")
        .isProvided("nbrDeformationPoint")
        .isProvided("electrodeLength")
        .isProvided("alphas")
        .isProvided("showAxis")
        .isProvided("showShape")
        .isProvided("showElectrodes")
        .isANumber("gridSize")
        .isStriclySuperior("gridSize", 1)
        .isANumber("positionX")
        .isANumber("positionY")
        .isANumber("nbrElectrodes")
        .isPositiveNumber("nbrElectrodes")
        .isANumber("electrodeLength")
        .isANumber("nbrDeformationPoint")
        .isStriclySuperior("nbrDeformationPoint", 1)
        .isStriclySuperior("electrodeLength", 1)
        .isStriclyInferior("electrodeLength", 101)
        .isABool("showAxis")
        .isABool("showShape")
        .isABool("showElectrodes")
        .getErrors()
}