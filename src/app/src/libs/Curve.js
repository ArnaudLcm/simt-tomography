export default function Curve(a, c1, c2, e, p5) {
	this.anchor = a;
	this.endpoint = e;
	this.control1 = c1;
	this.control2 = c2;

	this.bezierSelf = function() 
	{
		p5.bezier(this.anchor.x, this.anchor.y,
				this.control1.x, this.control1.y,
				this.control2.x, this.control2.y,
				this.endpoint.x, this.endpoint.y);
	}
x
	this.displayAnchors = function(color='red')
	{
		p5.strokeWeight(7);

		p5.stroke(color);
		p5.point(this.anchor.x, this.anchor.y);
		p5.point(this.endpoint.x, this.endpoint.y);
	}

	this.displayControls = function (color='blue')
	{
		p5.strokeWeight(14);

		p5.stroke(color);
		p5.point(this.control1.x, this.control1.y);
		p5.point(this.control2.x, this.control2.y);
	}

	this.displayPoints = function(color1='red', color2='blue')
	{
		this.displayAnchors(color1);
		this.displayControls(color2);
	}
}