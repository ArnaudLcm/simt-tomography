export default function Form(alphas, pos, p5)
{
    this.position = pos;
    this.alphas = alphas;
    this.N = (alphas.length - 1) / 2;

    this.equation = function(theta)
    {
        let res = 0;
        for(let k = 1; k <= this.N; ++k)
        {
            res += this.alphas[k] * Math.cos(k * theta) + this.alphas[k+this.N] * Math.sin(k*theta);
        }
        return res + this.alphas[0];
    }

    this.display = function(points = 1000)
    {
        p5.stroke(145,10, 200);
		p5.strokeWeight(3);
        for(let k = 0; k < points; ++k)
        {
            let theta = (2 * Math.PI * k) / points;
            let x = Math.cos(theta);
            let y = Math.sin(theta);
            let r = this.equation(theta);
            const posX = this.position.x
            const posY = this.position.y
            p5.point(posX + r * x, posY + r * y);
        }
    }
}