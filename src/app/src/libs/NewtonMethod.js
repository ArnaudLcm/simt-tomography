export function derivative(f, h = 0.001) {
    return function(x) { return (f(x + h) - f(x - h)) / (2 * h); };
}

export function newtonsMethod(f, firstGuess, precision) {
    let rec = function(guess, prevGuess)
    {
        if (guess === null || guess === undefined)
            guess = 0;
    
        if (Math.abs(prevGuess - guess) > precision) {
            return rec(guess - (f(guess) / derivative(f)(guess)), guess);
        } else {
            return guess;
        }
    }

    return rec(firstGuess , 0);
}