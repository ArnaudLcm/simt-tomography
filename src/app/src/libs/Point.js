export default function Point(x, y) {
	this.x = x;
	this.y = y;

	this.add = function (other)
	{
		this.x += other.x;
		this.y += other.y;
	}

	this.mult = function (a)
	{
		this.x *= a;
		this.y *= a;
	}

	this.addGet = function (other)
	{
    return new Point(this.x + other.x, this.y + other.y);
	}

	this.multGet = function (a)
	{
    return new Point(this.x * a, this.y * a);
	}
}