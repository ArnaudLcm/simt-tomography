import Point from "./Point"
import { distance, remapValue, vectorAngle } from "./Utils";



export function SplineShape(r, x, y, pCount, p5) {
    this.x = x;
    this.y = y;

    this.electrodes = []
    this.radius = r;
    this.realCount = pCount;

    this.displayControlPoints = true;

    this.controlPoints = [];

    //constructs the shape to first be a circle
    for (let p = 0; p <= pCount + 2; ++p) {
        let point = new Point((x + r * Math.cos(2 * Math.PI * p / pCount)),
            y + r * Math.sin(2 * Math.PI * p / pCount));

        this.controlPoints.push(point);
    }


    this.solveSystemEquation = function (systeme) {

        let matrix = systeme;
        for (let i = 0; i < matrix.length; i++) {
            // recherche de la ligne contenant le pivot
            let pivotRow = i;
            for (let j = i + 1; j < matrix.length; j++) {
                if (Math.abs(matrix[j][i]) > Math.abs(matrix[pivotRow][i])) {
                    pivotRow = j;
                }
            }

            // permutation des lignes pour mettre le pivot en haut
            [matrix[i], matrix[pivotRow]] = [matrix[pivotRow], matrix[i]];

            // division de la ligne pivot pour obtenir un 1 sur la diagonale
            const pivot = matrix[i][i];
            for (let j = i; j < matrix[i].length; j++) {
                matrix[i][j] /= pivot;
            }

            // élimination des coefficients en dessous du pivot
            for (let j = i + 1; j < matrix.length; j++) {
                const coefficient = matrix[j][i];
                for (let k = i; k < matrix[i].length; k++) {
                    matrix[j][k] -= coefficient * matrix[i][k];
                }
            }
        }

        // remontée pour éliminer les coefficients au dessus de la diagonale
        for (let i = matrix.length - 1; i >= 0; i--) {
            for (let j = i - 1; j >= 0; j--) {
                const coefficient = matrix[j][i];
                for (let k = i; k < matrix[i].length; k++) {
                    matrix[j][k] -= coefficient * matrix[i][k];
                }
            }
        }
        // extraction des solutions
        const solutions = matrix.map(row => row[row.length - 1]);
        for (let i = 0; i < solutions.length; i++) {
            if (solutions[i] < 10 ** (-4)) {
                solutions[i] = 0;
            }
        }
        return solutions;




    }

    this.resolveAlphas = function (alphaLen) {

        let thetas = [];
        let equations = [];
        for (let i = 0; i < alphaLen; i++) {
            //calcul des thetas egalement repartis sur la forme 
            let theta = (Math.PI * 2 / alphaLen) * i;
            thetas.push(theta);

            let equation = [1];
            for (let j = 1; j <= alphaLen / 2; j++) {
                equation.push(Math.sin(j * theta));
                equation.push(Math.cos(j * theta));
            }
            //calcul de distance entre le point d'angle theta et le centre de la forme
            let r = distance(this.thetaPoint(theta).x, this.thetaPoint(theta).y, this.x, this.y);
            equation.push(r);
            equations.push(equation);
        }

        return this.solveSystemEquation(equations)


    }



    // get the closest control point to the given coords
    this.getControlPointAt = function (x, y) {
        let mindist = this.radius;
        let cp = 0;
        for (let p = 0; p < this.controlPoints.length; ++p) {
            let newd = distance(x, y, this.controlPoints[p].x, this.controlPoints[p].y);
            if (newd < mindist) {
                mindist = newd;
                cp = p;
            }
        }
        let newd = distance(x, y, this.x, this.y);
        if (mindist < 20)
            return cp;
        if (newd < mindist) {
            return new Point(this.x, this.y);
        }
        return null;
    }

    this.getAnchor1At = function (theta) {
        let beta2 = 0;
        for (let p = 1; p < this.realCount; ++p) {
            beta2 = vectorAngle(this.controlPoints[p].x, this.controlPoints[p].y, this.x, this.y);
            if (theta - beta2 < 0) {
                // get corresponding polynom
                return this.getIndexesFor(p)[0];
            }
        }
        return this.getIndexesFor(this.realCount)[0];
    }

    // get the index of the nearest control point whose angle is greater than theta
    this.getAnchor2At = function (theta) {
        let beta2 = 0;
        for (let p = 1; p < this.realCount; ++p) {
            beta2 = vectorAngle(this.controlPoints[p].x, this.controlPoints[p].y, this.x, this.y);
            if (theta - beta2 < 0) {
                // get corresponding polynom
                return this.getIndexesFor(p)[1];
            }
        }
        return this.getIndexesFor(this.realCount)[1];
    }

    // move the indexth control point to the position (x,y)
    this.moveControlPoint = function (index, x, y) {
        this.controlPoints[index].x = x;
        this.controlPoints[index].y = y;

        if (index >= this.realCount) {
            this.controlPoints[index % this.realCount].x = x;
            this.controlPoints[index % this.realCount].y = y;
        }
        if (index < (this.controlPoints.length) % this.realCount) {
            this.controlPoints[index + this.realCount].x = x;
            this.controlPoints[index + this.realCount].y = y;
        }
    }

    this.getIndexesFor = function (i) {
        // works ?
        let i0 = 0;
        let i1 = 0;
        let i2 = i + 1;
        let i3 = i + 2;

        if (i === -1) {
            i0 = pCount - 2;
            i1 = pCount - 1;
            i2 = i + 1;
            i3 = i + 2;

        } else if (i === 0) {
            i0 = pCount - 1;
            i1 = i;

        } else {
            i0 = i - 1;//i > 0? i-1 : 
            i1 = i >= this.realCount ? 0 : i;
            i2 = i >= this.realCount ? 1 : i + 1;
            i3 = i >= this.realCount ? 2 : i + 2;
        }

        return [i0, i1, i2, i3];
    }
    // gives the polynomial equation between control point i and control point i + 1
    // p5 curveVertex is implemented with the uniform Catmull-Rom spline equations
    // (https://en.wikipedia.org/wiki/Centripetal_Catmull%E2%80%93Rom_spline with alpha = 0, hence the t0 = 0, t1 = 1, etc)
    this.splineAt = function (i) {

        let indexes = this.getIndexesFor(i);
        let i0 = indexes[0];
        let i1 = indexes[1];
        let i2 = indexes[2];
        let i3 = indexes[3];

        let P0 = this.controlPoints[i0];
        let P1 = this.controlPoints[i1];
        let P2 = this.controlPoints[i2];
        let P3 = this.controlPoints[i3];


        let t0 = 0;
        let t1 = 1;
        let t2 = 2;
        let t3 = 3;

        const A1 = (t) => P0.multGet((t1 - t) / (t1 - t0)).addGet(P1.multGet((t - t0) / (t1 - t0)));
        const A2 = (t) => P1.multGet((t2 - t) / (t2 - t1)).addGet(P2.multGet((t - t1) / (t2 - t1)));
        const A3 = (t) => P2.multGet((t3 - t) / (t3 - t2)).addGet(P3.multGet((t - t2) / (t3 - t2)));

        const B1 = (t) => A1(t).multGet((t2 - t) / (t2 - t0)).addGet(A2(t).multGet((t - t0) / (t2 - t0)));
        const B2 = (t) => A2(t).multGet((t3 - t) / (t3 - t1)).addGet(A3(t).multGet((t - t1) / (t3 - t1)));

        let spline = (t) => B1(t).multGet((t2 - t) / (t2 - t1)).addGet(B2(t).multGet((t - t1) / (t2 - t1)));
        let beta1 = vectorAngle(this.controlPoints[i1].x, this.controlPoints[i1].y, this.x, this.y);
        let beta2 = i2 === this.realCount ? 2 * Math.PI : vectorAngle(this.controlPoints[i2].x, this.controlPoints[i2].y, this.x, this.y);
        return (t) => spline(remapValue([beta1, beta2], [1, 2])(t));
    }

    // theta in ]-pi, pi]
    this.thetaPoint = function (theta) {
        let beta2 = 0;
        for (let p = 1; p < this.realCount; ++p) {
            beta2 = vectorAngle(this.controlPoints[p].x, this.controlPoints[p].y, this.x, this.y);
            if (theta - beta2 < 0) {
                // get corresponding polynom
                let spline = this.splineAt(p - 1);
                let inter = spline(theta);
                return inter;
            }
        }
        let spline = this.splineAt(this.realCount - 1);
        let inter = spline(theta);
        return inter;
    }

    this.display = function () {
        if (p5 != null) {

            // < -----     control points part     -----> 
            p5.stroke(145, 10, 200);
            p5.strokeWeight(7);

            if (this.displayControlPoints) {
                for (let p = 0; p < this.realCount; ++p) {
                    p5.point(this.controlPoints[p].x, this.controlPoints[p].y);
                }
            }

            // < ---------------------------------------> 

            // < -----          shape part         -----> 
            p5.stroke(42, 157, 143);
            p5.strokeWeight(2);
            p5.beginShape();
            let pCount = this.controlPoints.length;
            for (let p = 0; p < pCount; ++p) {
                p5.curveVertex(this.controlPoints[p].x, this.controlPoints[p].y);
            }
            p5.endShape();
            // < ---------------------------------------> 

            p5.stroke(42, 42, 143);
            p5.strokeWeight(4);
            for (let e = 0; e < this.electrodes.length; e++) {
                p5.beginShape();
                let ep1 = this.thetaPoint(this.electrodes[e].theta1);
                let ep2 = this.thetaPoint(this.electrodes[e].theta2);
                let istart = this.getAnchor1At(this.electrodes[e].theta1);
                let iend = this.getAnchor2At(this.electrodes[e].theta2);
                p5.curveVertex(this.controlPoints[istart].x, this.controlPoints[istart].y);
                p5.curveVertex(ep1.x, ep1.y);
                for (let cpe = istart + 1; cpe < iend; ++cpe) {
                    p5.curveVertex(this.controlPoints[cpe % pCount].x, this.controlPoints[cpe].y);
                }
                p5.curveVertex(ep2.x, ep2.y);
                p5.curveVertex(this.controlPoints[iend].x, this.controlPoints[iend].y);
                p5.endShape();
            }

        }


    }

    this.makeFromAlphas = function (alphas)
    {
        let rAlpha = function(theta)
        {
            let res = alphas[0];
            let N = Math.floor(alphas.length / 2);
            for(let i = 1; i <= N; ++i)
            {
                res += alphas[i] * Math.cos(i*theta) + alphas[i+N] * Math.sin(i*theta);
            }
            return res;
        }

        for(let p = 0; p < this.realCount; ++p)
        {
            let cptheta = vectorAngle(this.controlPoints[p].x, this.controlPoints[p].y, this.x, this.y);
            let rcp = rAlpha(cptheta);
            this.moveControlPoint(p, this.x + rcp * Math.cos(cptheta), this.y + rcp*Math.sin(cptheta));
        }
    }


    // returns the right angle of the Electorde
    this.getElectrodeRightAngle = function (theta1, l) {
        let dtheta = 2 * Math.PI / 360; // the step : equivalent to 1°
        let d = 0;
        let theta2 = theta1;
        let i1 = this.getAnchor1At(theta1) // index
        let next_CP = this.controlPoints[this.getAnchor2At(theta1)];
        let next_theta_CP = vectorAngle(next_CP.x, next_CP.y, this.x, this.y);
        let iteration = 0
        let f = this.splineAt(i1);
        while (d <= l) {
            while (theta2 < next_theta_CP) {
                theta2 = (theta2+dtheta)%(2*Math.PI);
                const p1 = f(theta1)
                const p2 = f(theta2)
                const detDistance = distance(p1.x, p1.y, p2.x, p2.y)
                d += detDistance;
                theta1 = theta2
                if(d > l)
                    return theta2
            }

            i1  = (i1+1)%this.realCount;
            iteration += 1
            if(iteration > 500) {
                return NaN
            }
            next_CP = this.controlPoints[this.getAnchor2At(theta1)];
            next_theta_CP = vectorAngle(next_CP.x, next_CP.y, this.x, this.y);
            f = this.splineAt(i1);
        }
        return theta2;
    }
}

export function Electrode(theta1, theta2) {
    this.theta1 = theta1 > theta2 ? theta2 : theta1;
    this.theta2 = theta1 > theta2 ? theta1 : theta2;
}
