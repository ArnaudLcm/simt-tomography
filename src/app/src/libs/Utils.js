export function distance_points(p1, p2)
{
    return Math.sqrt((p2.x-p1.x)**2 + (p2.y-p1.y)**2);
}

export function distance(x1, y1, x2, y2)
{
    return Math.sqrt((x2-x1)**2 + (y2-y1)**2);
}

export function getShapePos(windowsSize, gridSize, pos) {
    return pos*Math.floor(windowsSize/(gridSize*2)) + windowsSize/2
}


function dotProduct(x1, y1, x2, y2)
{
    return x1*x2 + y1 * y2;
}

function vectorNorm(vx, vy)
{
    return Math.sqrt(vx**2 + vy**2);
}

export function remapValue(fromRange, toRange) {
        const d = (toRange[1] - toRange[0]) / (fromRange[1] - fromRange[0]);
        return from =>  (from - fromRange[0]) * d + toRange[0];
}


// returns angle of vector drawn by point on a circle and circle center point
// angle is between -pi and pi
export function vectorAngle(vx, vy, cx, cy)
{
    let vsin = -(cy-vy);
    let vecx = vx-cx;
    let vecy = vy-cy;
    if(vsin > 0)
    {
        return Math.acos(dotProduct(vecx, vecy, 1, 0)/vectorNorm(vecx, vecy));
    } else
    {
        return (2 * Math.PI - Math.acos(dotProduct(vecx, vecy, 1, 0)/vectorNorm(vecx, vecy))) % (2*Math.PI);
    }
}
