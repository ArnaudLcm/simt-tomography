import {create} from "zustand"

export const useConfigStore = create((set, get) => ({
    config: null,
    updateConfig: (newConfig) => set((state) => {
        localStorage.setItem('simt', newConfig)
        return { ...state, config: newConfig }
    }),
    getConfig: () => get().config,
}))