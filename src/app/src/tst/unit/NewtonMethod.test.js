import {derivative,newtonsMethod } from "../../libs/NewtonMethod";

describe('Test derivative function', () => {
    test('Computes the derivative of a quadratic function', () => {
        let f = function(x) {
            return x ** 2
        }
        expect(derivative(f)(0)).toBeCloseTo(0);
    });

    test('Computes the derivative of a quadratic function', () => {
        let f = function(x) {
            return x ** 3
        }
        expect(derivative(f)(2)).toBeCloseTo(12);
    });

    test("Returns an infinite value of a non-differentiable function's derivative", () => {
        let f = function(x) {
            return 1/x 
        }
        expect(derivative(f)(0)).toBeGreaterThan(10**5);
    });

    test('Computes the derivative of a complex function', () => {
        let f = function(x) {
            return Math.exp(x)
        }
        expect(derivative(f)(5)).toBeCloseTo(f(5));
    });

    test('Computes the derivative of a complex function', () => {
        let f = function(x) {
            return Math.log(x)
        }
        expect(derivative(f)(0)).toBeNaN();
    });

    test('Computes the derivative of a complex function', () => {
        let f = function(x) {
            return Math.cos(x) + Math.sin(x)
        }
        expect(derivative(f)(10)).toBeCloseTo( Math.cos(10) - Math.sin(10));
    });

});

describe('Test NewtonsMethod function', () => {
    test('Checks the approximation of NewtonsMethod on a quadratic function', () => {
        let f = function(x) {
            return x ** 2
        }
        let precision = 0.001
        let firstGuess = 1
        expect(newtonsMethod(f,firstGuess,precision)).toBeCloseTo(0);
    });

    test('Checks the approximation of NewtonsMethod on a quadratic function with a firstGuess far from the solution', () => {
        let f = function(x) {
            return x ** 2
        }
        let precision = 0.001
        let firstGuess = 2000
        expect(newtonsMethod(f,firstGuess,precision)).toBeCloseTo(0);
    });

    test('Checks the approximation of NewtonsMethod on a quadratic function', () => {
        let f = function(x) {
            return x ** 2
        }
        let precision = 0.001
        let firstGuess = 2000
        expect(newtonsMethod(f,firstGuess,precision)).toBeCloseTo(0);
    });
    
    test('Checks the approximation of NewtonsMethod on a quadratic function', () => {
        let f = function(x) {
            return Math.sin(x) 
        }
        let precision = 0.001
        let firstGuess = 10
        expect(newtonsMethod(f,firstGuess,precision)).not.toBe(3.14);
    });

    test('Checks the approximation of NewtonsMethod on a quadratic function', () => {
        let f = function(x) {
            return Math.exp(x) 
        }
        let precision = 0.001
        let firstGuess = 10
        expect(newtonsMethod(f,firstGuess,precision)).toBeNaN();
    });

});