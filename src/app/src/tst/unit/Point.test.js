import Point from "../../libs/Point";

describe('Test add function', () => {
    test('Computes the addition of two integers', () => {
        let point1 = new Point(2,2);
        const point2 = new Point(2,3);
        
        point1.add(point2);

        expect(point1.x).toBe(4);
        expect(point1.y).toBe(5);
    });

    test('Computes the addition of two float numbers', () => {
        let point1 = new Point(2.5,2.3);
        let point2 = new Point(2.5,3.2);
        
        point1.add(point2);

        expect(point1.x).toBe(5);
        expect(point1.y).toBe(5.5);
    });

    test('Computes the addition of two numbers ', () => {
        let point1 = new Point(2.3,2);
        let point2 = new Point(-0.3,3);
        
        point1.add(point2);

        expect(point1.x).toBeCloseTo(2); // ça retourne 1.999 et pas 2
        expect(point1.y).toBe(5);
        //quand il s'agit de nombre négatif, les calculs ne sont pas exacts
    });
});

describe('Test mult function', () => {
    test('Computes the multiplication win an integer', () => {
        let point1 = new Point(2,3);
        
        point1.mult(2);

        expect(point1.x).toBe(4);
        expect(point1.y).toBe(6);
    });

    test('Computes the multiplication with a float', () => {
        let point1 = new Point(2,4);
        
        point1.mult(2.5);

        expect(point1.x).toBe(5);
        expect(point1.y).toBe(10);
    });

    test('Computes the multiplication of negative numbers', () => {
        let point1 = new Point(2,-4);
        
        point1.mult(-2.5);

        expect(point1.x).toBe(-5);
        expect(point1.y).toBe(10);
    });
});

describe('Test addGet function', () => {
    test('Computes the returned value of addition of two integers', () => {
        let point1 = new Point(2,2);
        const point2 = new Point(2,3);
        
        let point3 = point1.addGet(point2);
        
        expect(point3.x).toBe(4);
        expect(point3.y).toBe(5);
        expect(point3).toBeInstanceOf(Point);
    });

    test('Computes the returned value of addition of two float numbers', () => {
        let point1 = new Point(2.0,2.5);
        const point2 = new Point(2.6,3.7);
        
        let point3 = point1.addGet(point2);

        expect(point3.x).toBe(4.6);
        expect(point3.y).toBe(6.2);
        expect(point3).toBeInstanceOf(Point);  
    });

    test('Computes the returned value of addition of two negative float numbers', () => {
        let point1 = new Point(-2.0,2.5);
        const point2 = new Point(2.6,-3.7);
        
        let point3 = point1.addGet(point2);

        expect(point3.x).toBeCloseTo(0.6);
        expect(point3.y).toBeCloseTo(-1.2);
        expect(point3).toBeInstanceOf(Point); 
    });
});

describe('Test multGet function', () => {
    test('Computes the returned value of the multiplication of negative numbers', () => {
        let point1 = new Point(2,-4);
        
        let point3 = point1.multGet(-2.5);
        
        expect(point3.x).toBe(-5);
        expect(point3.y).toBe(10);
        expect(point3).toBeInstanceOf(Point);
    });

    test('Computes the returned value of the multiplication of positive numbers', () => {
        let point1 = new Point(2,4);
        
        let point3 = point1.multGet(2.5);
        
        expect(point3.x).toBe(5);
        expect(point3.y).toBe(10);
        expect(point3).toBeInstanceOf(Point);
    });
    
    test('Computes the returned value of the multiplication of positive numbers', () => {
        let point1 = new Point(2,4);
        
        let point3 = point1.multGet(0.1);
        
        expect(point3.x).toBe(0.2);
        expect(point3.y).toBe(0.4);
        expect(point3).toBeInstanceOf(Point);
    });
});