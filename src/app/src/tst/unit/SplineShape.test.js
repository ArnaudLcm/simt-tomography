import {Electrode, SplineShape} from "../../libs/SplineShape";
import Point from "../../libs/Point";
import {distance, getShapePos, vectorAngle} from "../../libs/Utils";
import Sketch from "react-p5";

/**
 * @jest-environment jsdom
 */


describe('Test valid construction function', () => {

    test('creates a SplineShape object with correct properties', () => {
        const spline = new SplineShape(10, 2, 1, 5, 5);
        expect(spline.radius).toEqual(10);
        expect(spline.x).toEqual(2);
        expect(spline.y).toEqual(1);
        expect(spline.realCount).toEqual(5);
        expect(spline.displayControlPoints).toBe(true);
        expect(spline).toBeInstanceOf(SplineShape);
    });

});

describe('Test getControlPointAt method', () => {

    test('Checks the nearest ControlPoint', () => {
        const x = 10;
        const y = 40;
        const pCount = 3;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        const controlPointIndex = splineShape.getControlPointAt(12,42);
        expect(controlPointIndex).toEqual(0);
    });

    test('Checks the nearest ControlPoint', () => {
        const x = 0;
        const y = 0;
        const pCount = 3;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);
        const controlPointIndex = splineShape.getControlPointAt(-2,-5);
        expect(controlPointIndex).toEqual(5);
    });

});

describe('Test getAnchorAt method', () => {
    test('Returns the correct Control Point with getAnchor1At', () => {
        const x = 5;
        const y = 0;
        const pCount = 4;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount,null);

        const theta = Math.PI / 2;
        let control_point_index = splineShape.getAnchor1At(theta);

        expect(control_point_index).toEqual(1);
    });

    test('Returns the correct Control Point with getAnchor2At', () => {
        const x = 5; // pt de contact de la forme avec l'axe des x
        const y = 0;
        const pCount = 4;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        const theta = Math.PI / 2;
        let control_point_index = splineShape.getAnchor2At(theta);

        expect(control_point_index).toEqual(2);
    });

});

describe('Test moveControlPoint method', () => {
    test('Returns the correct list of Control Points when index < pCount', () => {
        let x = 10
        let y = 40;
        const pCount = 3;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        const new_x = 25;
        const new_y = 55;
        const index = 0;
        splineShape.moveControlPoint(index,new_x,new_y);

        expect(splineShape.controlPoints[index]).toBeInstanceOf(Point);
        expect(splineShape.controlPoints[index].x).toBe(new_x);
        expect(splineShape.controlPoints[index].y).toBe(new_y);
    });

    test('Returns the correct list of Control Points when index =< pCount +  2', () => {
        let x = 10;
        let y = 40;
        const pCount = 3;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        const new_x = 0;
        const new_y = 0;
        const index = 3;
        splineShape.moveControlPoint(index,new_x,new_y);

        expect(splineShape.controlPoints[index%pCount]).toBeInstanceOf(Point);
        expect(splineShape.controlPoints[index%pCount].x).toBe(new_x);
        expect(splineShape.controlPoints[index%pCount].y).toBe(new_y);
    });
    test('Returns the correct list of Control Points when index > pCount +  2', () => {
        let x = 10;
        let y = 40;
        const pCount = 3;
        const radius = 7;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        const new_x = 0;
        const new_y = 0;
        const index = 3;
        splineShape.moveControlPoint(index,new_x,new_y);

        expect(splineShape.controlPoints[index%pCount]).toBeInstanceOf(Point);
        expect(splineShape.controlPoints[index%pCount].x).toBe(new_x);
        expect(splineShape.controlPoints[index%pCount].y).toBe(new_y);
    });
});


describe('Test thetaPoint method', () => {
    test('Returns the correct point corresponding to given theta', () => {
        let x = 0;
        let y = 0;
        const pCount = 3;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        let theta = 0;
        let point = splineShape.thetaPoint(0);

        expect(point).toBeInstanceOf(Point);
        expect(point.x).toBe(5);
        expect(point.y).toBe(0);

    });

    test('Returns the correct point corresponding to given theta', () => {
        let x = 0;
        let y = 0;
        const pCount = 8;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        let theta = Math.PI;
        let point = splineShape.thetaPoint(theta);

        expect(point).toBeInstanceOf(Point);
        expect(point.x).toBe(-5);
        expect(point.y).toBeCloseTo(0, 8);
    });
});


describe('Test getElectrodeRightAngle', () => {
    test('Form: perfect circle', () => {
        let x = 0;
        let y = 0;
        const pCount = 8;
        const radius = 5;
        const splineShape = new SplineShape(radius, x, y, pCount, null);

        expect(splineShape.getElectrodeRightAngle(0, 2*Math.PI*radius/4)).toBeCloseTo(Math.PI/2, 0)




    });

    test('Form: Multiple electrodes', () => {
        const circle = new SplineShape(10, 0, 0, 20, null);
        for (let i = 0; i < 5; i++) {
            let newTheta = (2*i*Math.PI)/5
            expect(circle.getElectrodeRightAngle(newTheta, 2*Math.PI*10/10)).toBeCloseTo(newTheta + 2*Math.PI/10, 0)
        }
    })

});

