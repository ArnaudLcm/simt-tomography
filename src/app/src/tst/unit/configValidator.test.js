import { ConfigValidator, validateConfig } from "../../libs/ConfigValigator";


test('Check a valid config', () => {
    let config = {
        test: 0
    }
    expect(new ConfigValidator(config).isProvided("test").isANumber("test").getErrors()).toStrictEqual({});
});


test('Config with a missing parameter', () => {
    let config = {
        test: 0
    }
    expect(new ConfigValidator(config).isProvided("test2").getErrors()).toStrictEqual({ "test2": ["The field test2 is required."], });
});

test('Config with an invalid parameter', () => {
    let config = {
        test: -1
    }
    expect(new ConfigValidator(config).isProvided("test").isPositiveNumber("test").getErrors()).toStrictEqual({ "test": ["The field test has to be a positive number."], });
});

