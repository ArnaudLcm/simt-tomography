cmake_minimum_required(VERSION 3.0)
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
project(main_wb)

add_subdirectory(class)

set(SRCS
    main.cpp
    )
    
add_executable(main ${SRCS})

target_compile_options(webserver PRIVATE)


target_link_libraries(main webserver Threads::Threads)
