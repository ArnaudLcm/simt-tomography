#include "APIController.hpp"
#include <string>
#include "../HTTPException.hpp"
#include <regex>
#include "../RegexFactory.hpp"

APIController::APIController(std::string method, std::string target): method(method), target(target) {}

std::string APIController::process() {
            if(regex_search(target, RegexFactory::URIMatcher("resolve"))) {
                return this->resolve();
            } else {
                throw HTTPException("404 No Method found", "NOT FOUND" ,NOT_FOUND); // In case where no method exists for this request
            }
}


std::string APIController::resolve() {
    return "No resolve";
}