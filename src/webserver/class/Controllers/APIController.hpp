#ifndef __APICONTROLLER_H
#define __APICONTROLLER_H
#include <string>
#include "Controller.hpp"

class APIController: public Controller {
    
    std::string method;
    std::string target;

public:
    APIController(const std::string method, const std::string target);
    ~APIController();

    std::string resolve(); 

    std::string process();

};

#endif