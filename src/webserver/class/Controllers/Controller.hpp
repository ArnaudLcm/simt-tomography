#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <string>

class Controller {
public:
    virtual std::string process();
};


#endif //CONTROLLER_H
