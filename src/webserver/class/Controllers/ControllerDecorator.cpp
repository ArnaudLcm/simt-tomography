#include "ControllerDecorator.hpp"
#include <sys/types.h>
#include <unistd.h>
#include "../HTTPException.hpp"
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include "stdlib.h"

#define BUFFER_RESPONSE_SIZE 10000

ControllerDecorator::ControllerDecorator(Controller *controller): controllerInstance(controller) {}

std::string ControllerDecorator::process() {

    int pid;
    char* shared = (char*)mmap(NULL, BUFFER_RESPONSE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, 0, 0);

    if((pid = fork()) < 0) { // Create a subprocess to handle request
        throw HTTPException("500 Internal Error Occured: can't create subprocess", "Internal Server Error" ,INTERNAL_SERVER_ERROR);
    }

    if(pid == 0) {

        std::string result;
        result = this->controllerInstance->process();

        for(std::string::size_type i = 0; i < result.size(); ++i) {
            shared[i] = result[i];
        }

        exit(0);

    }

    int childStatus;
    wait(&childStatus);
    if(WCOREDUMP(childStatus)) {
        throw HTTPException("500 Internal Error Occured : core dump", "Internal Server Error" ,INTERNAL_SERVER_ERROR);   
    }

    std::string resultProcess(shared);


    int errUnmap = munmap(shared, BUFFER_RESPONSE_SIZE);
    if(errUnmap < 0)
        throw HTTPException("500 Internal Error Occured : can't unmap", "Internal Server Error", INTERNAL_SERVER_ERROR);

    return resultProcess;

}
