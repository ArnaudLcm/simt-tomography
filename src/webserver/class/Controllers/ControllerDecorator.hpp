#ifndef CONTROLLERDECORATOR_H
#define CONTROLLERDECORATOR_H
#include "Controller.hpp"
#include <string>


/**
 * @brief The purpose of this decorator is to run every controller method in a subprocess in order to keep
 * safe the webserver if a segfault occurs in a controller.
*/
class ControllerDecorator : public Controller {

protected:
    Controller* controllerInstance;

public:
    ControllerDecorator(Controller* controller);

    std::string process();

};


#endif //CONTROLLERDECORATOR_H
