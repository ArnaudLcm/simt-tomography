#ifndef __HTTPEXCEPTION_H
#define __HTTPEXCEPTION_H
#include <iostream>
enum HTTP_RESPONSE_CODE {
    OK=200,
    UNAUTHORIZED=300,
    BAD_REQUEST=400,
    METHOD_NOT_ALLOWED=405,
    NOT_FOUND=404,
    LENGTH_REQUIRED=411,
    INTERNAL_SERVER_ERROR=500,
};


class HTTPException : public std::exception {
	const char* msg;
    const char* msg_code;
    int codeResponse;
    
    public:
    	HTTPException(const char* msg, const char* msg_code, const int code) : exception(),
        	msg (msg),
        	codeResponse (code),
            msg_code (msg_code)
        {}
        
        const char* get_msg() const { return msg; }
        const char* get_msg_code() const {return msg_code;}
        int get_code() const { return codeResponse; }     
};
#endif