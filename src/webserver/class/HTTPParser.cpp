#include "HTTPParser.hpp"
#include <iostream>
#include <string.h>
#include <algorithm>
#include <regex>
#include "HTTPException.hpp"

bool isAlphaNumeric(const std::string &str)
{
    auto it = std::find_if_not(str.begin(), str.end(), [](char const &c) {
            return isalnum(c);
        });
 
    return it == str.end();
}

/**
 * Verify if the HTTP request is valid (Check RFC format) and fill a map with arguments
*/
HTTPParser::HTTPParser(char* buff, int buff_size): buffer(new char[buff_size+1]) {
    strcpy(buffer, buff);
    

    int methodLength = strcspn(buffer, " ");
    if(methodLength > 7)
        throw std::invalid_argument("Unknown method");
    
    if(memcmp(buffer, "GET", methodLength) == 0) {
        httpHash["method"] = "GET";
    } else if(memcmp(buffer, "POST", methodLength) == 0) {
        httpHash["method"] = "POST";
    } else if(memcmp(buffer, "PUT", methodLength) == 0) {
        httpHash["method"] = "PUT";
    } else if(memcmp(buffer, "PATCH", methodLength) == 0) {
        httpHash["method"] = "PATCH";
    }else if(memcmp(buffer, "HEAD", methodLength) == 0) {
        httpHash["method"] = "HEAD";
    } else if(memcmp(buffer, "DELETE", methodLength) == 0) {
        httpHash["method"] = "DELETE";
    } else if(memcmp(buffer, "CONNECT", methodLength) == 0) {
        httpHash["method"] = "CONNECT";
    } else if(memcmp(buffer, "OPTIONS", methodLength) == 0) {
        httpHash["method"] = "OPTIONS";
    } else if(memcmp(buffer, "TRACE", methodLength) == 0) {
        httpHash["method"] = "TRACE";
    }else {
        throw HTTPException("This method is not allowed", "Method Not Allowed", METHOD_NOT_ALLOWED);
    }

    buffer += (methodLength + 1); // Ptr to next location
    
    while (buffer[0] == ' '){//eliminate all the spaces
        buffer+=1;
    }
    int uriLength = strcspn(buffer, " ");
    std::string token;
    char* uriPtr = (char*)malloc(uriLength*sizeof(char));
    char* uriFinalPtr = (char*)malloc(uriLength*sizeof(char)); // This will contain the full URI (for example : /test/welcome)
    memcpy(uriFinalPtr, buffer, uriLength);
    std::string uriFinal = uriFinalPtr;
    
    if(uriLength > 1) {
        /**
         * The purpose of this block is to verify if names in the URI are alphanumeric.
         * @example : /test/test2 is valid
         *          : /test?/test2 is rejected
        */
        memcpy(uriPtr, buffer, uriLength);


        std::string uri = ++uriPtr; // Skip the first /
        size_t pos = 0;

        while (pos != std::string::npos) {
            pos = uri.find("/");
            if(pos == std::string::npos) { // Prevent unknow behaviour
                token = uri.substr(0, uri.length());
            } else {
                token = uri.substr(0, pos);

            }
            if(!isAlphaNumeric(token)) {
                throw HTTPException("400 Bad request", "BAD REQUEST" ,BAD_REQUEST);
            }

            uri.erase(0, pos + 1);
        }
    }

    uriFinal[uriLength] = '\0';
    httpHash["URI"] =  uriFinal;
}

std::string HTTPParser::getMethod() {
    return httpHash["method"];
}

std::string HTTPParser::getRequestURI() {
    return httpHash["URI"];
}

/**
 * The purpose of this function is to retrieve URI's last part
 * @example : getTargetFromURI("/api/test") will return test
 * @return Return the string or throw an exception if the URI is invalid to retrieve the target.
*/
std::string  HTTPParser::getTargetFromURI(std::string URI) {
    const std::size_t indexLastSeparator = URI.find_last_of("/");
    if (indexLastSeparator != std::string::npos)
    {
        std::string lastPartUrl = URI.substr(indexLastSeparator+1);
        return lastPartUrl;
    }
    throw std::invalid_argument("No target in the URI");

}


HTTPParser::~HTTPParser() {
    delete  buffer;
}