#ifndef __HTTPPARSER_H
#define _HTTPPARSER_H
#include <map>
#include <string>
class HTTPParser {
private:
    char* buffer;
    std::map<std::string, std::string> httpHash;

public:
    HTTPParser(char* buff, int buff_size);
    ~HTTPParser();

    int isRequestValid();
    std::string getMethod();
    std::string getRequestURI();

    std::string static getTargetFromURI(std::string URI);
};


#endif