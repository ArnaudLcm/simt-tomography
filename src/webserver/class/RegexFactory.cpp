#include "RegexFactory.hpp"
#include <string>
#include <regex>
#include <sstream>

std::regex RegexFactory::URIMatcher(std::string base) {
    std::ostringstream regexString;
    regexString << "^" << base;
    std::regex regURI(regexString.str());
    return regURI;
}

std::regex RegexFactory::HTTPCodeGroup(int n) {
    if(n < 0 || n > 5) {
        throw std::invalid_argument("Unknown http code group.");
    }
    std::ostringstream regexString;
    regexString << "^" << n << "[0-9]{2}";
    std::regex reg(regexString.str());
    return reg;
}