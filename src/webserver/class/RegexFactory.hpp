#ifndef __REGEXFACTORY_H
#define __REGEXFACTORY_H
#include <regex>

/**
 * The purpose of this class is to store some useful regex expression
*/
class RegexFactory {
public:
    static std::regex URIMatcher(std::string base);

    static std::regex HTTPCodeGroup(int n);

};
#endif