#include "Rooter.hpp"
#include <regex>
#include "RegexFactory.hpp"
#include "Controllers/APIController.hpp"
#include "HTTPException.hpp"
#include "HTTPParser.hpp"
#include "Controllers/ControllerDecorator.hpp"

/**
 * Handle the request from the user by orienting the request to the specific controller
*/
std::string Rooter::handle(std::string method, std::string request) {

    if(regex_search(request, RegexFactory::URIMatcher("/api"))) {
        ControllerDecorator* apiC = new ControllerDecorator(new APIController(method, HTTPParser::getTargetFromURI(request)));
        return apiC->process();
    }

    throw HTTPException("404 No Root found", "NOT FOUND" ,NOT_FOUND); // In case where not root exists for this request


}