#ifndef __ROOTER_H
#define __ROOTER_H
#include <string>
class Rooter {
public:
    //Rooter();// il faut soit inmplémenter le constrcuteur et destructeur dans le .cpp ou enlever sa déclaration dans le .hpp
    //~Rooter(); 

    std::string static handle(std::string method, std::string request);

};
#endif // !__ROOTER_H
