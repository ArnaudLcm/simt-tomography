#include "WebServer.hpp"

int main(int argc, char const *argv[])
{
    WebServer* ws = new WebServer();
    ws->start();
    return 0;
}
