#include "Utils.hpp"
#include "stdio.h"
#include "stdlib.h"
#include "RegexFactory.hpp"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <regex>


#define RST  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

int Utils::exit_with_failure(const std::string message, bool persist) {

    std::cerr << "[Error] " << message << std::endl;
    
    if(persist) {
        std::ofstream outfile ("errors-webserver.txt");
        outfile << "[Error] " << message << std::endl;
        outfile.close();
    }
    return EXIT_FAILURE;

}


int Utils::get_http_code_color(std::string code) {
    if(regex_search(code, RegexFactory::HTTPCodeGroup(5))) {
        return InfoType::ERROR;
    }

    if(regex_search(code, RegexFactory::HTTPCodeGroup(2))) {
        return InfoType::SUCCESS;
    }

    return -1;
}

void Utils::print_info(int type, std::string title, std::string message) {
    std::ostringstream formatedOutput;

    formatedOutput << "[";
    switch(type) {
        case InfoType::SUCCESS:
            formatedOutput << KGRN;
            break;
        case InfoType::WARNING:
            formatedOutput << KYEL;
            break;
        case InfoType::ERROR:
            formatedOutput << KRED;
            break;
        default:
            formatedOutput << KBLU;
            break;
    }
    formatedOutput << title << "]: " << message;

    std::cout << formatedOutput.str();
}