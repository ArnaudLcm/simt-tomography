#ifndef __UTILS_H
#define __UTILS_H
#include <iostream>
#include <string>

enum InfoType {
    SUCCESS,
    WARNING,
    ERROR
};

class Utils {

public:
    int static exit_with_failure(const std::string message, bool persist=false);
    void static print_info(int type, std::string title, std::string message);
    int static get_http_code_color(const std::string code);
    Utils() {}
    ~Utils() {}
};

#endif