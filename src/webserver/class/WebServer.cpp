#include "WebServer.hpp"    
#include "Utils.hpp"
#include <arpa/inet.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include "Rooter.hpp"
#include "HTTPException.hpp"
#include <thread>

struct HTTP_RESPONSE
{
    float http_version;
    std::string content_type;
    int content_length;
};

typedef struct { int sockfd; } thread_config_t;



/**
 * @brief The method will be used in a sub thread for every incoming request
 * In this method, the response (writting in the socket) to the client is also done.
**/
void* WebServer::handleRequest(void* args)
{
    thread_config_t* config = (thread_config_t*)args;
    int fd_socket = config->sockfd;
    free(config);
    char buff_request[BUFFER_REQUEST_SIZE] = {0};

    int bytes_received = read(fd_socket, buff_request, BUFFER_REQUEST_SIZE);
    if (bytes_received == -1)
        Utils::exit_with_failure("An error occured while reading the incoming request.");
    HTTPParser *hp;
    std::ostringstream response;
    std::string responseBody;

    try
    {
        hp = new HTTPParser(buff_request, sizeof(buff_request));
    }
    catch (const HTTPException &e)
    {
        response << "HTTP/1.1 " << e.get_code() << " " << e.get_msg_code() << "\nContent-Type: text/html\nContent-Length: " << strlen(e.get_msg()) << "\n\n"
                 << e.get_msg();
    }

    
    if(response.str().length() == 0) {
        try
        {
            responseBody = Rooter::handle(hp->getMethod(), hp->getRequestURI());

#ifdef DEBUG_MODE
            Utils::print_info(Utils::get_http_code_color("200"), "200", "The request has been handle with success.");
#endif
            response << "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: " << responseBody.length() << "\n\n"
                    << responseBody;
        }
        catch (const HTTPException &e)
        {
            response << "HTTP/1.1 " << e.get_code() << " " << e.get_msg_code() << "\nContent-Type: text/html\nContent-Length: " << strlen(e.get_msg()) << "\n\n"
                    << e.get_msg();
            
        }
    }


    int bytes_written = write(fd_socket, response.str().c_str(), sizeof(response));
    if (bytes_written < 0)
        Utils::exit_with_failure("An error occured while writing in the socket.");
    
    return 0;

}

int WebServer::start()
{
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0)
        return Utils::exit_with_failure("An error occured while trying to create the server socket.");

    std::cout << "[------ Webserver starting... ------]" << std::endl;

    socket_adress.sin_port = htons(LISTEN_PORT); // Assign to the socket a listening port
    socket_adress.sin_family = AF_INET;          // Assign the family of protocols
    socket_adress.sin_addr.s_addr = INADDR_ANY;  // Assign the ip adress

    int bindServer = bind(socket_fd, (sockaddr *)&socket_adress, socket_adress_len);
    if (bindServer < 0)
        return Utils::exit_with_failure("An error occured while trying to bind the socket.");

    if (listen(socket_fd, PENDING_QUEUE_LENGTH) < 0)
    {
        return Utils::exit_with_failure("An error occured while trying to listen to new requests");
    }

    std::cout << "[------ Webserver is now listing on port " << LISTEN_PORT << " ------]" << std::endl;


    while (true)
    { // Loop through incoming requests
        int current_conn_socket = accept(socket_fd, (sockaddr *)&socket_adress, &socket_adress_len);

        if (current_conn_socket < 0)
            return Utils::exit_with_failure("An error occured while accepting an incoming request");

        
        pthread_t p;
        thread_config_t* config = (thread_config_t*)malloc(sizeof(*config)); // Structure which contains every args provided to handleRequest + respect pthread_create typing
        config->sockfd = current_conn_socket;
        pthread_create(&p, NULL, WebServer::handleRequest, config);

    }
    return EXIT_SUCCESS;

}

WebServer::WebServer() : socket_adress_len(sizeof(socket_adress)) {}

WebServer::~WebServer()
{
    close(socket_fd);
}

int WebServer::stop() {
    close(socket_fd);
    return EXIT_SUCCESS;
}