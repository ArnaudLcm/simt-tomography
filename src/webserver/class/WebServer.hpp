#ifndef __WEBSERVER_H
#define __WEBSERVER_H

#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <errno.h>
#include "HTTPException.hpp"
#include "HTTPParser.hpp"
#define LISTEN_PORT 8000 // Port to listen
#define PENDING_QUEUE_LENGTH 30 // Max number of clients in the listening pending queue
#define BUFFER_REQUEST_SIZE 30720
#define BUFFER_RESPONSE_SIZE 30720



/**
 * @brief The purpose of this class is to be able to init easily the web server which will handle requests from
 * front app (In the case of usage, the react app) 
*/
class WebServer {

private:
    int socket_fd; // Will contain the ip socket
    struct sockaddr_in socket_adress;
    unsigned int socket_adress_len;


public:
    WebServer();
    
    ~WebServer();
    int start();

    int stop();

    static void* handleRequest(void* args);
};

#endif