#include "class/WebServer.hpp"
#include "class/Utils.hpp"


int main(int argc, char const *argv[])
{
    WebServer* ws = new WebServer();
    ws->start();
    return 0;
}
