#include <gtest/gtest.h>
#include "../class/Controllers/APIController.hpp"
#include <string>

TEST(APIController, processExecption)
{
    EXPECT_THROW({
         try
        {
            std::string target("first test");
            std::string method("Unknown");
            APIController * c= new APIController(method, target);
            c->process();
        }

        catch (std::invalid_argument &e)
        {
            std::cout << e.what() << std::endl;
        }
    },
                 std::exception);

}
TEST(APIController, process)
{
    
    std::string target("resolve something");//string that start whit resolve
    std::string method("Unknown");
    APIController * c= new APIController(method, target);
    EXPECT_TRUE(c->process()== c->resolve()); 
    
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}