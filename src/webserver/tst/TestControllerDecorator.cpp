#include <gtest/gtest.h>
#include "../class/HTTPException.hpp"
#include "../class/Controllers/ControllerDecorator.hpp"
#include "../class/Controllers/APIController.hpp"
#include <iostream>
#include <cassert>
#include <cstring>
#include <sys/mman.h>
#include <signal.h>




TEST(ControllerDecoratorTest, testsubprocess ){
    ControllerDecorator c = new ControllerDecorator(new APIController("POST", "resolve"));
    try{
        std::string result = c.process();
    }
    catch(const HTTPException &e){
        FAIL() << e.get_msg() ;
    }
}

TEST(ControllerDecoratorTest, testprocess ){
    std::string method("GET");
    std::string target("resolve");
    APIController *APIC = new APIController(method, target);//the process and resolve return a "No resolve"
    ControllerDecorator c = new ControllerDecorator(APIC);
    std::string resultProcess = c.process();
    std::string expected("No resolve");
    EXPECT_TRUE(resultProcess == expected);
}

//making sure the child throws an exception
TEST(ControllerDecoratorTest, processChildError ){
    std::string method("GET");
    std::string target("error");
    APIController *APIC = new APIController(method, target);//the process and resolve return a "No resolve"
    ControllerDecorator c = new ControllerDecorator(APIC);
    int pid = fork();
    try
    {
        std::string resultProcess = c.process();
        
        std::string expected("No resolve");
        EXPECT_FALSE(resultProcess == expected);
    }
    catch (const HTTPException &e)
    {
        if(pid==0){
        EXPECT_STREQ(e.get_msg_code(),"NOT FOUND");
        }
    }
}


int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}