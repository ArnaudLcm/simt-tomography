#include <gtest/gtest.h>
#include "../class/HTTPParser.hpp"
#include "../class/HTTPException.hpp"
#include <iostream>
#include <cassert>
#include <cstring>

TEST(HTTPParser, testInstanciationInvalid)
{
    EXPECT_THROW({
        char buff[] = "Hello World !";
        try
        {
            HTTPParser *p = new HTTPParser(buff, sizeof(buff));
        }

        catch (std::invalid_argument &e)
        {
            std::cout << e.what() << std::endl;
        }
    },
                 std::exception);
}

TEST(HTTPParser, testInstanciationUnknowMethod)
{
    char buff[] = "PEWPEW /test/test+";
    try
    {
        HTTPParser *p = new HTTPParser(buff, sizeof(buff));
        FAIL() << "Expected HTTPException";
    }

    catch (const HTTPException &e)
    {
        EXPECT_STREQ(e.get_msg_code(),"Method Not Allowed");
    }
}

TEST(HTTPParser, testInstanciationInvalidURI)
{
    char buff[] = "GET /test/test+";
    try
    {
        HTTPParser *p = new HTTPParser(buff, sizeof(buff));
        FAIL() << "Expected HTTPException";
    }

    catch (const HTTPException &e)
    {
        EXPECT_STREQ(e.get_msg_code(), "BAD REQUEST");
    }
}

TEST(HTTPParser, testRetrieveParameters)
{
    char buff[] = "GET /test/test";
    
    HTTPParser *p = new HTTPParser(buff, sizeof(buff));
    EXPECT_TRUE(p->getMethod() == "GET");
    EXPECT_TRUE(p->getRequestURI() == "/test/test");
}


TEST(HTTPParser, testGetLastPartOfURI)
{
    char buff[] = "PUT /ORG/tot/titi/test12";

    HTTPParser *p = new HTTPParser(buff, sizeof(buff));
    ASSERT_TRUE(p->getMethod() == "PUT");
    ASSERT_TRUE(p->getRequestURI() == "/ORG/tot/titi/test12");
    EXPECT_TRUE(p->getTargetFromURI(p->getRequestURI()) == "test12");
        
}

TEST(HTTPParser, testGetTargetFromInvalidURI)
{
    char buff[] = "PATCH /test/test";
    std::string URI("test");
    try
    {
        HTTPParser *p = new HTTPParser(buff, sizeof(buff));
        std::string LastPartURI = p->getTargetFromURI(URI) ;
        FAIL() << "Expected invalid argument exception";
    }

    catch (const std::invalid_argument &e)
    {
        EXPECT_STREQ(e.what(), "No target in the URI");
    }
}
TEST(HTTPParser, testEliminateSpaces)
{
    char buff[] = "TRACE     /test/test";

    
    HTTPParser *p = new HTTPParser(buff, sizeof(buff));
    EXPECT_TRUE(p->getMethod()=="TRACE");
    EXPECT_TRUE(p->getRequestURI()=="/test/test");
    
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}