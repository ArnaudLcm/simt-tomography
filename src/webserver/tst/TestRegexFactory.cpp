#include <gtest/gtest.h>
#include "../class/RegexFactory.hpp"
#include <iostream>
#include <cstring>
#include <regex>

TEST(RegexFactoryTest, URIMatcherTestValidURI) {
    RegexFactory regexFactory;
    std::string base = "https://www.test.com";
    std::regex regURI = regexFactory.URIMatcher(base);


    std::string validURI = "https://www.test.com/path/to/resource?query=value";
    bool isMatch = std::regex_search(validURI, regURI);
    EXPECT_TRUE(isMatch);
}

TEST(RegexFactoryTest, URIMatcherTestDifferentBaseURI) {
    RegexFactory regexFactory;
    std::string base = "https://www.test.com";
    std::regex regURI = regexFactory.URIMatcher(base);

    std::string differentBaseURI = "https://www.example.com/path/to/resource?query=value";
    bool isMatch = std::regex_search(differentBaseURI, regURI);
    EXPECT_FALSE(isMatch);
}

TEST(RegexFactoryTest, URIMatcherTestInvalidURI) {
    RegexFactory regexFactory;
    std::string base = "https://www.test.com";
    std::regex regURI = regexFactory.URIMatcher(base);

    std::string invalidURI = "htps:/www.test.com/path/to/resource?query=value";
    bool isMatch = std::regex_search(invalidURI, regURI);
    EXPECT_FALSE(isMatch);
}

TEST(RegexFactory, HTTPCodeGroupUnknown)
{
    try
    {
        RegexFactory * regex= new RegexFactory();
        regex->HTTPCodeGroup(100);
    }
    catch (std::invalid_argument &e)
    {
        EXPECT_STREQ(e.what(), "Unknown http code group.");
    }

}

TEST(RegexFactory, HTTPCodeGroupKnown)
{
    try
    {
        RegexFactory * regex= new RegexFactory();
        regex->HTTPCodeGroup(0);
    }
    catch (std::invalid_argument &e)
    {
        std::cout << e.what() << std::endl;
    }

}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}