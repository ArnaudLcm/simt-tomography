#include <gtest/gtest.h>
#include "../class/Rooter.hpp"
#include <string>
#include "../class/HTTPException.hpp"

TEST(Rooter, handle_error)
{
    try
    {
        std::string method("meth");
        std::string request("api");
        Rooter r;
        r.handle(method, request);
    }
    catch (const HTTPException &e)
    {
        EXPECT_STREQ(e.get_msg_code(),"NOT FOUND");
    }
}

TEST(Rooter, handle_api_match)
{
    
    std::string method("GET");
    std::string request("/api/resolve");
    Rooter r;
    std::string apiC = r.handle(method, request);
    std::string expected("No resolve");
    EXPECT_TRUE(apiC == expected);
   
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}