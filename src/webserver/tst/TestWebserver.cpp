#include <gtest/gtest.h>
#include "../class/HTTPException.hpp"
#include "../class/WebServer.hpp"
#include <iostream>
#include <cassert>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include<thread>
#include "../class/Utils.hpp"


std::string create_client(std::string request, int socketfd){
    struct sockaddr_in socket_adress;

    socket_adress.sin_family = AF_INET;
    socket_adress.sin_port = htons(8000);
    socket_adress.sin_addr.s_addr = INADDR_ANY;

    int bindServer = connect(socketfd, (sockaddr *)&socket_adress, sizeof(socket_adress));
    if(bindServer < 0) {
        Utils::exit_with_failure("Can't connect the socket");
    }
    
    int writeOperation = write(socketfd, request.c_str(), request.length());
    if(writeOperation < 0) {
        Utils::exit_with_failure("Can't write in the socket.");
    }

    send(socketfd, request.c_str(), request.length(), 0);

    char* buffer = (char*)malloc(sizeof(char)*255);

    int n = read(socketfd,buffer,255);
    if (n < 0) {
        Utils::exit_with_failure("Fail on reading the response");
    }

    std::string temp(buffer);
    return temp;
}

TEST(WebServer, InvalidHTTPRequest) {

    int pid;
    if((pid = fork()) < 0) {
        FAIL() << "Can't create a subprocess.";
    }

    if(pid == 0) {
        WebServer* ws;
        int r = ws->start();

        if(r != 0) {
            FAIL() << "Can't launch webserver.";
        }

        exit(0);
    } else {
        int pid2 = fork();
        if((pid2) < 0) {
        FAIL() << "Can't create a subprocess.";
        }

        else if (pid2 == 0){
            sleep(3);
            int socket_fd1 = socket(AF_INET, SOCK_STREAM, 0);


            if(socket_fd1 < 0) {
                FAIL() << "Can't create a socket.";
            }


            std::string request1 = " /hello/resolve";
            std::string temp;
            try{
                 temp = create_client(request1, socket_fd1);//(buffer);
            }
            catch(const HTTPException &e){
                std::ostringstream expected;
                expected << "HTTP/1.1 " << e.get_code() << " " << e.get_msg_code() << "\nContent-Type: text/html\nContent-Length: " << strlen(e.get_msg()) << "\n\n"
                 << e.get_msg();
                ASSERT_EQ(temp, expected.str());        
            }
            close(socket_fd1);
        }
        else{
            sleep(3);
            int socket_fd2 = socket(AF_INET, SOCK_STREAM, 0);


            if(socket_fd2 < 0) {
                FAIL() << "Can't create a socket.";
            }

            std::string request2 = "POST /hello/resolve";
            std::string temp2;
            try{
                temp2 = create_client(request2, socket_fd2);//(buffer);
            }
            catch(const HTTPException &e){
                std::ostringstream expected;
                expected << "HTTP/1.1 " << e.get_code() << " " << e.get_msg_code() << "\nContent-Type: text/html\nContent-Length: " << strlen(e.get_msg()) << "\n\n"
                 << e.get_msg();
                ASSERT_EQ(temp2, expected.str());        
            }
            
            close(socket_fd2);
        }

    }

}


TEST(WebServer, WebServerINIT) {

    int pid;
    if((pid = fork()) < 0) {
        FAIL() << "Can't create a subprocess.";
    }

    if(pid == 0) {
        WebServer* ws;
        int r = ws->start();

        if(r != 0) {
            FAIL() << "Can't launch webserver.";
        }

        exit(0);
    } else {


        sleep(3);
        int socket_fd = socket(AF_INET, SOCK_STREAM, 0);


        if(socket_fd < 0) {
            FAIL() << "Can't create a socket.";
        }


        std::string request = "GET /api/resolve";

        std::string temp = create_client(request, socket_fd);
        EXPECT_TRUE(temp == std::string("HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 10\n\nNo resolve"));
        close(socket_fd);

    }

}

TEST(WebServerTest, MultipleClients)
{
    int pid;
    if((pid = fork()) < 0) {
        FAIL() << "Can't create a subprocess.";
    }
    if(pid == 0) {
        WebServer* ws;
        int r = ws->start();

        if(r != 0) {
            FAIL() << "Can't launch webserver.";
        }

        exit(0);
    } else {
        int pid2 = fork();
        if (pid2<0){
            FAIL() << "Can't create a subprocess.";
        }
        else if (pid2 == 0){
            int socketfd1 = socket(AF_INET, SOCK_STREAM, 0);
            if(socketfd1 ==-1){
                FAIL() << "socket can't connect";
            }
    

            std::string request1 = "GET /api/resolve";


            std::string temp1 = create_client(request1, socketfd1);//(buffer);
            ASSERT_EQ(temp1, std::string("HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 10\n\nNo resolve")); 
            close(socketfd1);
        }
        else {
            int socketfd2 = socket(AF_INET, SOCK_STREAM, 0);
            if(socketfd2 ==-1){
                FAIL() << "socket can't connect";
            }
    

            std::string request1 = "POST /api/test2/resolve";


            std::string temp2 = create_client(request1, socketfd2);//(buffer);
            ASSERT_EQ(temp2, std::string("HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 10\n\nNo resolve")); 

            close(socketfd2);
        }
    }
}


int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}