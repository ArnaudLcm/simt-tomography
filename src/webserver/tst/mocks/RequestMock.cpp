#include "gmock/gmock.h"
#include "gmock/gmock.h"



class Solver {
public:
    virtual ~Solver() {}
    virtual void solve( int N, unsigned int formRadius, unsigned int nbElectrodes ) = 0;
    virtual void exportMetrics( char *fileName ) const = 0;
    virtual void exportResults( char *fileName ) const = 0;
};


class MockSolver: Solver {
 public:
  MOCK_METHOD(void, solve, ( int N, unsigned int formRadius, unsigned int nbElectrodes ), (override));
  MOCK_METHOD(void, exportMetrics, (char *fileName), (const, override));
  MOCK_METHOD(void, exportResults, (char *fileName), (const, override));
};
